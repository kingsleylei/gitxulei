#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter

def get_diff_order():
    today = datetime.now().date()
    total_sql = """
    select date(CONVERT_TZ(po.create_time,'+00:00','+8:00')) as create_date,po.uuid,sku_no,item_no,title,qty_system_demand,
    qty_purchased,purchase_company_name,sku_difficult_note,purchase_note,state,source_type,difficult_order_follower_name
    from purchase_order as po
    left join purchase_order_notes as pon on po.id=pon.purchase_order_id
    left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    where difficult_order=1 and state=0 and purchase_note not regexp '有货|已登记'
    """
    # total_sql="""
    # select po.uuid,content,CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(log.create_time,'+00:00','+8:00') as log_time,sku_no,item_no,title,qty_system_demand,qty_purchased,purchase_company_name,sku_difficult_note,purchase_note,state,source_type,difficult_order_follower_name,difficult_order
    # from purchase_order as po
    # join log_logmessage as log on po.id=log.object_id
    # left join purchase_order_notes as pon on po.id=pon.purchase_order_id
    # left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    # left join product as pro on sku.product_id=pro.id
    # where object_type=1  and date(CONVERT_TZ(log.create_time,'+00:00','+8:00'))<'{}'
    # and difficult_order=1 and state=0 and purchase_note  not regexp '有货|已登记'
    # """.format(today)
    sql="""
    select po.uuid,content,CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(log.create_time,'+00:00','+8:00') as log_time,
    date(CONVERT_TZ(log.create_time,'+00:00','+8:00')) as log_date,sku_no,item_no,title,qty_system_demand,qty_purchased,purchase_company_name,
    sku_difficult_note,purchase_note,state,source_type,difficult_order_follower_name,difficult_order,difficult_order_follower_name
	from purchase_order as po 
	join log_logmessage as log on po.id=log.object_id
	left join purchase_order_notes as pon on po.id=pon.purchase_order_id
	left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
	left join product as pro on sku.product_id=pro.id
	where object_type=1  and date(CONVERT_TZ(log.create_time,'+00:00','+8:00'))>='{}'
	and content regexp "取消 -> 疑难|购买备注 -> \'有货|购买备注 -> \'已登记"
    """.format(today-timedelta(days=15))
    #得到<=T-1所有的疑难单,必须早上早点来运行
    df_total=get_raw_DataSet(total_sql)
    df_total['purchase_note'] = df_total['purchase_note'].str.replace(r'[=+-@]', '')
    df_total['sku_difficult_note'] = df_total['sku_difficult_note'].str.replace(r'[=+-@]', '')
    diff_total_num = df_total['uuid'].count()
    #得到T-1当天的所有生成的疑难单,包括多次进入,处理好的疑难单
    df_all=get_raw_DataSet(sql)
    df_all['purchase_note'] = df_all['purchase_note'].str.replace(r'[=+-@]', '')
    df_all['sku_difficult_note'] = df_all['sku_difficult_note'].str.replace(r'[=+-@]', '')

    df_diff=df_all[df_all['content'].str.find(u'取消 -> 疑难')!=-1].drop_duplicates('uuid', keep='first')#生成的疑难单
    df_diff=df_diff[df_diff['log_date']==today-timedelta(days=1)]
    diff_uni_num =df_diff['uuid'].count()
    #diff_uni_num =df_diff['uuid'].drop_duplicates().size  # 生成的疑难单
    diff0_uni_num = df_diff.loc[df_diff['source_type']==0,'uuid'].count()
    diff2_uni_num = df_diff.loc[df_diff['source_type'] == 2, 'uuid'].count()

    df_instock=df_all[df_all['content'].str.find(u'有货')!=-1].drop_duplicates('uuid',keep='first')
    df_instock=df_instock[df_instock['log_date']==today-timedelta(days=1)]
    instock_num=df_instock['uuid'].count()

    df_reg=df_all[df_all['content'].str.find(u'已登记')!=-1].drop_duplicates('uuid',keep='first')
    df_reg=df_reg[df_reg['log_date']==today-timedelta(days=1)]
    reg_num=df_reg['uuid'].count()

    df_rawT=pd.concat([df_diff,df_instock,df_reg],ingnore_index=True)
    df_stat = pd.DataFrame(
        { u'T-1疑难': diff_uni_num,u'T-1系统单生成':diff0_uni_num,u'T-1补单生成':diff2_uni_num,u'<=T-1疑难': diff_total_num,
        u'已登记完成':reg_num, u'有货完成':instock_num}, index=[datetime.now().date()],
        columns=[u'T-1疑难',u'T-1系统单生成',u'T-1补单生成',u'<=T-1疑难',u'已登记完成',u'有货完成'])
    # #print df_all.head()
    return df_total,df_rawT,df_stat
def get_raw_DataSet(sql):
    warehouse = [u'余杭', u'萧山', u'海宁']
    df_all = pd.DataFrame()
    for con, wh in zip(conn, warehouse):
        df = pd.read_sql(sql, con)
        df['warehouse'] = wh
        df_all = df_all.append(df, ignore_index=True)
    return df_all
if __name__ == '__main__':

    conn=db_help.get_idoo_conn()

    df_rawT,df_total,df_stat=get_diff_order()

    path = u'F:\createExcel\疑难日报'+str(datetime.now().date())+'.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_total.to_excel(writer, sheet_name=u'<=T-1疑难', index=False, encoding='utf8')
    df_rawT.to_excel(writer, sheet_name=u'T-1疑难', index=False, encoding='utf8')
    df_stat.to_excel(writer, sheet_name=u'统计', index=True, encoding='utf8')
    writer.save()