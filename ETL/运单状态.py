#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter


def get_so_state(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
	select so.order_name,CONVERT_TZ(so.create_at,'+00:00','+8:00') as order_date,tracking_no,delivery_state,state 	as order_status from 
	sale_logistics as sl 
	join sale_order as so on sl.order_name=so.order_name
	where delivery_way='Delhivery' or state='cancel'

    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    return df_all
if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()

    df=get_so_state(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    # #print df_all.head()
    path = u'F:\createExcel\运单状态.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df.to_excel(writer, sheet_name=u'大货运单', index=False, encoding='utf8')
    writer.save()