#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import db_help
import xlsxwriter


def get_pr_info(start, end, idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    SELECT pr.uuid,pr.supplier_name,CONVERT_TZ(pr.create_time,'+00:00','+8:00') as create_time ,CONVERT_TZ(pr.pay_time,'+00:00','+8:00') as pay_time ,pr.purchase_rmb,pr.freight_rmb,pr.total_rmb,pr.pay_rmb,real_received,pr.status,pr.purchase_note
    from
	payment_request as pr 
	join 
	(select pr.uuid,sum(po.qty_received) as real_received
	from payment_request as pr 
	join pr_bill as pb on pr.id=pb.pr_id
	join bill on pb.bill_id=bill.id
	join bill_purchase_order as bpo  on bill.id=bpo.bill_id
	join purchase_order as po on bpo.po_id=po.id
	group by pr.uuid) as t on pr.uuid =t.uuid
    where CONVERT_TZ(pr.create_time,'+00:00','+8:00') BETWEEN '{}' and '{}'
    """.format(start,end)

    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df['freight_rmb']=np.where((df['freight_rmb']==0)&(df['purchase_note'].str.find(u'运费')!=-1),df['pay_rmb']-df['purchase_rmb'],df['freight_rmb'])
    df_cate=get_company_cate(datetime.now().date()-timedelta(days=60),datetime.now().date(),idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    df_addr=get_company_addr()
    df=df.merge(df_cate,on='supplier_name',how='left')
    df=df.merge(df_addr,on='supplier_name',how='left')
    return  df
 #贴上时间加快查询速度，
def get_company_cate(start, end, idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select  purchase_company_name,category_name 
    from purchase_order as po
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    where po.create_time BETWEEN '{}' and '{}'
    """.format(start, end)
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df=df.drop_duplicates('purchase_company_name', keep='last')
    df.rename(columns={'purchase_company_name':'supplier_name'},inplace=True)

    return df
def get_company_addr():
    supp_conn=db_help.get_idoo_supplier()
    sql="""
    SELECT company_name,biz_place
    FROM supplier
    """
    df=pd.read_sql(sql,supp_conn)
    df['biz_place']=df['biz_place'].str.replace(u'中国','')
    df['biz_place']=df['biz_place'].str.replace('/',' ')
    df['biz_place']=df['biz_place'].str.strip()
    df['biz_place']=df['biz_place'].str.split(' ') #得到列表

    #列表中有Null需要判断
    df['province']=map(lambda s:s[0] if isinstance(s,list) else np.NaN,df['biz_place'])
    df['city'] = map(lambda s: s[1] if isinstance(s, list) and len(s)>=2 else np.NaN, df['biz_place'])
    df.rename(columns={'company_name': 'supplier_name'}, inplace=True)
    df=df[['supplier_name','province','city']]
    df = df.drop_duplicates('supplier_name')
    return df
if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()

    start = '2017-11-01'
    end = '2017-11-30'
    start = datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.strptime(end, "%Y-%m-%d").date()

    df=get_pr_info(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)

    path = r'F:\createExcel\pr_info6.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df.to_excel(writer, sheet_name=u'请款单', index=False, encoding='utf8')
    writer.save()
