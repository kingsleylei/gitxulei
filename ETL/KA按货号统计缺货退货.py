#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
import datetime
import db_help
import xlsxwriter
def get_po_info(start,end,idoo_yh_conn ,idoo_xs_conn,idoo_hn_conn):
    sql="""
    select date_format(CONVERT_TZ(po.create_time,'+00:00','+8:00'),"%Y-%m-%d") as create_date,po.uuid,qty_system_demand,qty_purchase_demand,qty_purchased,qty_received,supply_qyt_sys_demand,supply_qyt_purchased,
    purchase_price,qty_purchased*purchase_price as purchase_amount, CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(signed_time,'+00:00','+8:00') as signed_time_ch,timestampdiff(SECOND,po.create_time,purchase_time)/3600 as order_interval,
   purchase_company_name,is_cooperative_supplier,apply_quantity,return_quantity,gr.status,sku_no,item_no,source_type,po.state,po.prev_state,purchase_link,category_name
    from
    (select * from
    (select *
    from purchase_order
    where  CONVERT_TZ(create_time,'+00:00','+8:00') between '{}' and '{}' and source_type in (0,2) and state !=7) as b
    left join 
    (select source_purchase_order_id as sp_order_id,sum(qty_system_demand) as supply_qyt_sys_demand,sum(qty_purchased) as supply_qyt_purchased
    from purchase_order group by source_purchase_order_id
    ) as c on b.id=c.sp_order_id) as po 
    left join 	
    goods_return as gr on po.id=gr.purchase_order_id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all.drop_duplicates('uuid')
   # df_all['qty_real_demand']=np.where(df_all['qty_system_demand']!=0,df_all['qty_purchased'],max(df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_all['qty_real_demand'] = np.where(df_all['qty_purchased'] != 0, df_all['qty_purchased'],np.where(df_all['qty_system_demand'] != 0, df_all['qty_system_demand'],
                                np.where(df_all['supply_qyt_sys_demand'] > df_all['supply_qyt_purchased'],df_all['supply_qyt_sys_demand'], df_all['supply_qyt_purchased'])))
    # 特殊情况系统需求0，采购0，未产生补单，取人工需求
    df_all['qty_real_demand'] = np.where((pd.isnull(df_all['qty_real_demand'])) | (df_all['qty_real_demand'] == 0),
                                         df_all['qty_purchase_demand'], df_all['qty_real_demand'])
    df_all.loc[df_all['status']==3,'apply_quantity']=0
    df_storage = get_storage_time(start, end, idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    df_all = df_all.merge(df_storage, on='uuid', how='inner')
    df=df_all

    df['create_time'] = pd.to_datetime(df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['real_logi_time'] = pd.to_datetime(df['real_logi_time'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    #转换成小时数
    df['logistics_interval']=map(lambda x,y:(x-y).total_seconds()/3600,df['signed_time'],df['purchase_time'])

    df=df[df['is_cooperative_supplier']==1]

    grouped=df.groupby('item_no',as_index=False)
    df=grouped.agg({'qty_real_demand':'sum','qty_system_demand':'sum','qty_purchased':'sum','qty_received':'sum','apply_quantity':'sum','order_interval':'sum',
                    'logistics_interval':'sum','uuid':'count','purchase_link':'first','purchase_amount':'sum','category_name':'first'})

    df['shortage_rate']=(df['qty_real_demand']-df['qty_received'])/df['qty_real_demand']
    df['return_rate']=df['apply_quantity']/df['qty_real_demand']

    df['avg_order_interval']=df['order_interval']/df['uuid']
    df['avg_logistics_interval']=df['logistics_interval']/df['uuid']
    df['avg_purchased_price']=df['purchase_amount']/df['qty_purchased']
    df=df.sort_values(by='shortage_rate',ascending=True)
    df=df[['item_no','qty_real_demand','qty_received','avg_purchased_price','shortage_rate','return_rate','avg_order_interval','avg_logistics_interval','purchase_link','category_name']]
    return df

def get_storage_time(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    SELECT po.uuid,CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(po.purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(st.create_time,'+00:00','+8:00') as person_logi_time, CONVERT_TZ(st2.create_time,'+00:00','+8:00') as sys_logi_time,CONVERT_TZ(po.signed_time,'+00:00','+8:00') as signed_time,
    CONVERT_TZ(po.complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(storage_time,'+00:00','+8:00') as storage_time,state,is_cooperative_supplier
    from
    purchase_order as po 
    left join purchase_order_shipping_tickets as post on po.id=post.purchaseorder_id
    left join shipping_ticket as st on post.shippingticket_id=st.id
    left join supply_order as so on po.supply_order_id=so.id
    left join shipping_ticket_supply_orders as stso on so.id=stso.supplyorder_id
    left join shipping_ticket as st2 on stso.shippingticket_id=st2.id
    left join
    (select object_id,max(create_time) as storage_time
    from log_logmessage  
    where content like '%入库%'
    group by object_id
    ) as logl on po.id=logl.object_id
    where po.create_time between '{}' and '{}' and po.source_type in (0,2) 
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    #保留时间物流时间较早的
    df.drop_duplicates('uuid',inplace=True)

    df['create_time'] = pd.to_datetime(df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['person_logi_time'] = pd.to_datetime(df['person_logi_time'])
    df['sys_logi_time'] = pd.to_datetime(df['sys_logi_time'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])
    #物流单号生成时间
    df['real_logi_time']=np.where((pd.notnull(df['person_logi_time']))&(pd.notnull(df['sys_logi_time'])),np.where(df['person_logi_time']<df['sys_logi_time'],df['person_logi_time'],df['sys_logi_time']),
                                  np.where(pd.isnull(df['person_logi_time']),df['sys_logi_time'],df['person_logi_time']))
    #未采购 已采购 全部缺货情况下没有入库时间
    df.loc[(df['state'] == 0)|(df['state'] == 1)|(df['state'] == 6),'storage_time'] = pd.NaT
    #无签收时间取入库时间
    df['signed_time'] = np.where(pd.isnull(df['signed_time']), df['storage_time'], df['signed_time'])

    df['real_logi_time']=np.where(pd.isnull(df['purchase_time']),pd.NaT,df['real_logi_time'])
    # KA的发货时间：有发货的，定义为创建时间采购时间发货
    df['real_logi_time'] = np.where((df['is_cooperative_supplier'] == 1)&(pd.notnull(df['signed_time']))& (pd.isnull(df['real_logi_time'])),df['purchase_time'],df['real_logi_time'])

    #print df['real_logi_time'].dtype
    #非KA无物流时间，取签收时间和购买时间的中值
    df['real_logi_time'] = np.where((df['is_cooperative_supplier'] == 0)&(pd.notnull(df['purchase_time'])) & (pd.notnull(df['signed_time'])) & (pd.isnull(df['real_logi_time'])),df['purchase_time'] + (df['signed_time'] - df['purchase_time']) / 2, df['real_logi_time'])
    #经测试，物流时间有可能早于采购时间，只好取采购时间
    df['real_logi_time'] = np.where(df['real_logi_time'] < df['purchase_time'], df['purchase_time'],df['real_logi_time'])


    #df.to_excel(u'F:\createExcel\采购完成率test2原来原来.xlsx', index=False, encoding='utf8')

    df=df[['uuid','real_logi_time','signed_time','storage_time']]

    return df
def company_sku(idoo_yh_conn ,idoo_xs_conn,idoo_hn_conn,supplier_conn):

    #supply info
    sql="""
    select sku_id,sku_no,item_no
    from stock_keep_unit as sku join product as pro on sku.product_id=pro.id
    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all=df_all.drop_duplicates()

    supply_sql="""
    select company_name,stock_keep_unit_id as sku_id
    from link join sku_link_rel as sll on link.id=sll.link_id

    """
    df_sup=pd.read_sql(supply_sql,supplier_conn)
    df=df_sup.merge(df_all,on='sku_id',how='inner')
    df['company_name']= df['company_name'].str.strip()
    grouped=df.groupby('company_name',as_index=False)
    df=grouped['sku_no','item_no'].agg(lambda arr:arr.drop_duplicates().size)
    df.to_excel('F:\createExcel\company_sku2.xlsx')
    #print df.head(10)
    # df=df.merge(df_sup,left_on='purchase_company_name',right_on='company_name',how='left')
    # df['item_out_rate']=df['sku_id']/df['sku_num']

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()
    start='2017-11-01'
    end='2017-11-07'
    start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
    df_all=get_po_info(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    #company_sku(idoo_yh_conn,idoo_xs_conn,idoo_hn_conn,conn)

    path = r'F:\createExcel\shortageByItem6'+str(start)+'-'+str(end)+'.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_all.to_excel(writer, index=False, encoding='utf8')
    writer.save()