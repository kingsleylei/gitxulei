#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
import datetime
import xlwt
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from cStringIO import StringIO
import sys
reload(sys)
sys.setdefaultencoding('utf8')
base_path = os.path.dirname(os.path.realpath(__file__))
base_path

def send_mail(excel_files, recipients):
    username = 'mengnizhang@epiclouds.net'
    password = 'ZMN123zju'
    sender = username
    receivers = ", ".join(recipients)

    # 如名字所示： Multipart就是多个部分
    msg = MIMEMultipart()
    msg['Subject'] = 'Daily Data Report'
    msg['From'] = sender
    msg['To'] = receivers

    # 正文
    puretext = MIMEText('Attached please find the latest data report.', _charset='UTF-8')
    msg.attach(puretext)

    # xlsx类型的附件
    for excel_file in excel_files:
        try:
            xlsxpart = MIMEApplication(open(excel_file, 'rb').read())
            xlsxpart.add_header('Content-Disposition', 'attachment',
                                filename=excel_file.split('/')[-1].encode('gbk'))
            msg.attach(xlsxpart)
        except IOError:
            print '要添加的附件本身不存在'

    try:
        host = 'smtp.mxhichina.com'
        port = 465
        client = smtplib.SMTP_SSL(host, port)
        client.login(username, password)
        client.sendmail(sender, receivers, msg.as_string())
        client.quit()
        print 'yeah'
    except smtplib.SMTPRecipientsRefused:
        print 'Recipient refused'
    except smtplib.SMTPAuthenticationError:
        print 'Auth error'
    except smtplib.SMTPSenderRefused:
        print 'Sender refused'
    except smtplib.SMTPException, e:
        print e.message
    except Exception, e:
        print e.message


if __name__ == '__main__':
    today = str(datetime.date.today())
    yesterday = str(datetime.date.today() - datetime.timedelta(1))

    shanniwang_files = []
    shanniwang_files.append(base_path + "/output/{0}上周品类分析数据.xls".format(today))

    lingnaguo_files = []
    lingnaguo_files.append(base_path + "/output/{0}上货商品简要信息.xls".format(yesterday))
    lingnaguo_files.append(base_path + "/output/{0}下货商品简要信息.xls".format(yesterday))
    lingnaguo_files.append(base_path + "/output/近4日idoo中的销售信息(于{0}早上4点40读取).xls".format(today))

    nanhuaxu_files = []
    nanhuaxu_files.append(base_path + "/output/{0}new_user.xlsx".format(yesterday))
    nanhuaxu_files.append(base_path + "/output/{0}上周品类分析数据.xls".format(today))
    nanhuaxu_files.append(base_path + "/output/{0}上货商品简要信息.xls".format(yesterday))
    nanhuaxu_files.append(base_path+'/output/{}固定价格商品.xls'.format(today))
    nanhuaxu_files.append(base_path + "/output/{}商品数量统计表(早4点统计结果).xlsx".format(str(today)))
    nanhuaxu_files.append(base_path + "/output/{0}category_product_week.csv".format(today))
    nanhuaxu_files.append(base_path+ "/output/{0}_rating_result.csv".format(yesterday))
    nanhuaxu_files.append(base_path+ "/output/{0}user_commnt.csv".format(yesterday))


    luliu_files = []
    luliu_files.append(base_path + "/output/{0}上货商品简要信息.xls".format(yesterday))

    wenlongma_files = []
    wenlongma_files.append(base_path + "/output/{0}_shipped_products_detail.csv".format(yesterday))
    wenlongma_files.append(base_path + "/output/{0}_refunded_products_detail.csv".format(yesterday))
    wenlongma_files.append(base_path + "/output/{0}_paid_products_detail.csv".format(yesterday))
    wenlongma_files.append(base_path+ "/output/{0}_rating_result.csv".format(yesterday))
    wenlongma_files.append(base_path+ "/output/{0}user_commnt.csv".format(yesterday))

    huihuang_files = []
    huihuang_files.append(base_path + "/output/近4日idoo中的销售信息(于{0}早上4点40读取).xls".format(today))


    linjieli_files = []
    linjieli_files.append(base_path + "/output/{0}上周品类分析数据.xls".format(today))
    linjieli_files.append(base_path + "/output/{0}category_product_week.csv".format(today))
    linjieli_files.append(base_path+ "/output/{0}_rating_result.csv".format(yesterday))
    linjieli_files.append(base_path+ "/output/{0}user_commnt.csv".format(yesterday))
    linjieli_files.append(base_path+ "/output/{0}app_order_info.xlsx".format(str(today)))

    jungao_files = []
    jungao_files.append(base_path+ "/output/{0}_rating_result.csv".format(yesterday))



    yunlou_files = []
    yunlou_files.append(base_path+ "/output/{0}_rating_result.csv".format(yesterday))
    yunlou_files.append(base_path+"/output/{0}buy_again_results.xlsx".format(today))

    xiaoliangxu_files = []
    xiaoliangxu_files.append(base_path + "/output/近4日idoo中的销售信息(于{0}早上4点40读取).xls".format(today))
    xiaoliangxu_files.append(base_path + "/output/{0}_shipped_products_detail.csv".format(yesterday))
    xiaoliangxu_files.append(base_path + "/output/{0}_refunded_products_detail.csv".format(yesterday))
    xiaoliangxu_files.append(base_path + "/output/{0}_paid_products_detail.csv".format(yesterday))

    feipeng_files = []
    feipeng_files.append(base_path + "/output/{}new_user_month.xlsx".format(today))
    feipeng_files.append(base_path + "/output/近4日idoo中的销售信息(于{0}早上4点40读取).xls".format(today))
    minghuilin_files = []
    minghuilin_files.append(base_path + "/output/近4日idoo中的销售信息(于{0}早上4点40读取).xls".format(today))

    yanyanli_files = []
    yanyanli_files.append(base_path + "/output/近4日idoo中的销售信息(于{0}早上4点40读取).xls".format(today))

    caiwu_files = []
    caiwu_files.append(base_path + "/output/{0}_shipped_products_detail.csv".format(yesterday))
    caiwu_files.append(base_path + "/output/{0}_refunded_products_detail.csv".format(yesterday))
    caiwu_files.append(base_path + "/output/{0}_paid_products_detail.csv".format(yesterday))

    india_files = []
    india_files.append(base_path + "/output/{0}_India_paid_products_detail.csv".format(yesterday))
    india_files.append(base_path + "/output/{0}_India_refunded_products_detail.csv".format(yesterday))
    india_files.append(base_path + "/output/{0}_India_paid_products_detail.csv".format(yesterday))


    india_finance_files = []
    india_finance_files.append(base_path + "/output/{0}_India_shipped_products_detail.csv".format(yesterday))
    india_finance_files.append(base_path + "/output/{0}_India_refunded_products_detail.csv".format(yesterday))
    india_finance_files.append(base_path + "/output/{0}_India_paid_products_detail.csv".format(yesterday))

    minghuazhu_files = []
    minghuazhu_files.append(base_path + "/output/{}商品数量统计表(早4点统计结果).xlsx".format(str(today)))

    zixuanhe_files = []
    zixuanhe_files.append(base_path + "/output/{0}上货商品简要信息.xls".format(yesterday))
    zixuanhe_files.append(base_path + "/output/{0}下货商品简要信息.xls".format(yesterday))
    zixuanhe_files.append(base_path + "/output/{0}上周品类分析数据.xls".format(today))
    zixuanhe_files.append(base_path + "/output/{0}new_user.xlsx".format(yesterday))
    zixuanhe_files.append(base_path + "/output/{}商品数量统计表(早4点统计结果).xlsx".format(str(today)))

    wentaohuang_files = []
    wentaohuang_files.append(base_path + "/output/{0}上货商品简要信息.xls".format(yesterday))
    wentaohuang_files.append(base_path + "/output/{0}下货商品简要信息.xls".format(yesterday))
    wentaohuang_files.append(base_path + "/output/{0}上周品类分析数据.xls".format(today))
    wentaohuang_files.append(base_path + "/output/{0}new_user.xlsx".format(yesterday))
    wentaohuang_files.append(base_path + "/output/{0}_shipped_products_detail.csv".format(yesterday))
    wentaohuang_files.append(base_path + "/output/{0}_refunded_products_detail.csv".format(yesterday))
    wentaohuang_files.append(base_path+'/output/{}固定价格商品.xls'.format(today))
    wentaohuang_files.append(base_path + "/output/{}商品数量统计表(早4点统计结果).xlsx".format(str(today)))
    wentaohuang_files.append(base_path + "/output/{0}category_product_week.csv".format(str(today)))

    haoranzhang_files = []
    haoranzhang_files.append(base_path+"/output/{0}buy_again_results.xlsx".format(today))
    haoranzhang_files.append(base_path+ "/output/{0}_rating_result.csv".format(yesterday))

    zizinzhang_files=[]
    zizinzhang_files.append(base_path + "/output/{0}new_user.xlsx".format(yesterday))

    jialunli_files=[]
    jialunli_files.append(base_path+ "/output/{0}_rating_result.csv".format(yesterday))
    jialunli_files.append(base_path+"/output/{0}buy_again_results.xlsx".format(today))

    liuli_files=[]
    liuli_files.append(base_path + "/output/近4日idoo中的销售信息(于{0}早上4点40读取).xls".format(today))

    tingtinghong_files=[]
    tingtinghong_files.append(base_path + "/output/{}_po_sku_results.csv".format(yesterday))
    tingtinghong_files.append(base_path + "/output/{}_purchase_results.csv".format(yesterday))

    jinfengyuan_files = []
    jinfengyuan_files.append(base_path + "/output/{}_purchase_results.csv".format(yesterday))

    xuema_files = []
    xuema_files.append(base_path + "/output/{}_po_sku_results.csv".format(yesterday))

    beileixing_files =[]
    beileixing_files.append(base_path + "/output/{}new_old_user_rate.csv".format(str(today)))
    beileixing_files.append(base_path+  "/output/{}app_order_info.xlsx".format(str(today)))

    send_mail(shanniwang_files, ['shanniwang@epiclouds.net'])
    send_mail(lingnaguo_files, ['lingnaguo@epiclouds.net'])
    send_mail(nanhuaxu_files, ['nanhuaxu@epiclouds.net'])
    send_mail(luliu_files, ['luliu@epiclouds.net'])
    send_mail(wenlongma_files, ['wenlongma@epiclouds.net'])
    send_mail(huihuang_files, ['huihuang@epiclouds.net'])
    send_mail(linjieli_files, ['linjieli@epiclouds.net'])
    send_mail(jungao_files, ['jungao@epiclouds.net'])
    send_mail(yunlou_files, ['yunlou@epiclouds.net'])
    send_mail(xiaoliangxu_files, ['xiaoliangxu@epiclouds.net'])
    send_mail(feipeng_files, ['feipeng@epiclouds.net'])
    send_mail(yanyanli_files, ['yanyanli@epiclouds.net'])
    send_mail(caiwu_files, ['caiwu@epiclouds.net'])
    send_mail(india_files, ['india@epiclouds.net'])
    send_mail(india_finance_files, ['priyanka@epiclouds.net'])
    send_mail(minghuazhu_files, ['minghuazhu@epiclouds.net'])
    send_mail(zixuanhe_files, ['zixuanhe@epiclouds.net'])
    send_mail(wentaohuang_files, ['wentaohuang@epiclouds.net'])
    send_mail(haoranzhang_files,['haoranzhang@epiclouds.net'])
    send_mail(zizinzhang_files,['zixinzhang@epiclouds.net'])
    send_mail(jialunli_files,['jialunli@epiclouds.ne'])
    send_mail(liuli_files,['liliu@epiclouds.net'])
    send_mail(tingtinghong_files,['tingtinghong@epiclouds.net'])
    send_mail(xuema_files,['xuema@epiclouds.net'])
    send_mail(beileixing_files,['beileixing@epiclouds.net'])
    send_mail(jinfengyuan_files,['jinfengyuan@epiclouds.net'])
    send_mail(minghuilin_files,['minghuilin@epiclouds.net'])


