#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter

pd.options.mode.chained_assignment = None  # default='warn'
#获取物流时间和入库时间
def get_storage_time(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    SELECT po.uuid,CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(po.purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(st.create_time,'+00:00','+8:00') as logistics_createtime,CONVERT_TZ(person_createTime,'+00:00','+8:00')as person_createTime,
    CONVERT_TZ(po.signed_time,'+00:00','+8:00') as signed_time,CONVERT_TZ(po.complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(storage_time,'+00:00','+8:00') as storage_time,state
    from
    purchase_order as po left join supply_order as supo on supply_order_id=supo.id
    left join shipping_ticket_supply_orders as stso on supo.id=stso.supplyorder_id
    left join shipping_ticket as st on stso.shippingticket_id=st.id
    left join
    (select object_id,max(create_time) as person_createTime
    from log_logmessage 
    where content like '%人工物流单号%'
	GROUP BY object_id) as log on po.id =log.object_id
    left join
    (select object_id,min(create_time) as storage_time
    from log_logmessage  
    where content like '%入库%'
    group by object_id
    ) as logl on po.id=logl.object_id
    where po.create_time between '{}' and '{}' and po.source_type in (0,2) 
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)

    df['create_time'] = pd.to_datetime(df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['logistics_createtime'] = pd.to_datetime(df['logistics_createtime'])
    df['person_createTime'] = pd.to_datetime(df['person_createTime'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])


    df.loc[(df['state'] == 0)|(df['state'] == 1)|(df['state'] == 6), 'storage_time'] = pd.NaT
    df['real_logi_time'] = np.where(pd.isnull(df['logistics_createtime']), df['person_createTime'],df['logistics_createtime'])
    df['signed_time'] = np.where(pd.isnull(df['signed_time']), df['storage_time'], df['signed_time'])

    # print df.dtypes
    df['real_logi_time'] = np.where(
        (pd.notnull(df['purchase_time'])) & (pd.notnull(df['signed_time'])) & (pd.isnull(df['real_logi_time'])),
        df['purchase_time'] + (df['signed_time'] - df['purchase_time']) / 2, df['real_logi_time'])
    df['real_logi_time'] = np.where(df['real_logi_time'] < df['purchase_time'], df['purchase_time'],
                                    df['real_logi_time'])
    df=df[['uuid','real_logi_time','signed_time','storage_time']]
    return df
def get_po_rawdata(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    select date_format(CONVERT_TZ(po.create_time,'+00:00','+8:00'),"%Y-%m-%d") as create_date,po.uuid,qty_system_demand,qty_purchase_demand,qty_purchased,qty_received,supply_qyt_sys_demand,supply_qyt_purchased,
    purchase_price,qty_purchased*purchase_price as purchase_amount, CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(signed_time,'+00:00','+8:00') as signed_time_ch,timestampdiff(SECOND,po.create_time,purchase_time)/3600 as order_interval,
    purchase_company_name,is_cooperative_supplier,apply_quantity,gr.status,sku_no,item_no,source_type,po.state,po.prev_state,po.follow_user_name,category_name
    from
    (select * from
    (select *
    from purchase_order
    where CONVERT_TZ(create_time,'+00:00','+8:00') between '{}' and '{}' and source_type in (0,2) and state !=7) as b
    left join 
    (select source_purchase_order_id as sp_order_id,sum(qty_system_demand) as supply_qyt_sys_demand,sum(qty_purchased) as supply_qyt_purchased
    from purchase_order group by source_purchase_order_id
    ) as c on b.id=c.sp_order_id) as po 
    left join 	
    goods_return as gr on po.id=gr.purchase_order_id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)

   # df_all['qty_real_demand']=np.where(df_all['qty_system_demand']!=0,df_all['qty_purchased'],max(df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_all['qty_real_demand'] = np.where(df_all['qty_system_demand']!=0,np.where(df_all['qty_system_demand']>df_all['qty_purchased'],df_all['qty_system_demand'],df_all['qty_purchased']),
                                         np.where(df_all['supply_qyt_sys_demand']>df_all['supply_qyt_purchased'],df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_temp=df_all.pop('qty_real_demand')
    df_all.insert(8,'qty_real_demand',df_temp)
    #取得完整的采购时间轴
    df_storage=get_storage_time(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn)
    df=df_all.merge(df_storage,on='uuid',how='inner')
    df = df.drop_duplicates('uuid', keep='last')

    del df['signed_time_ch']
    df.loc[df['status']==3,'apply_quantity']=0
    df=df[(df['is_cooperative_supplier']==1)&(df['source_type']==0)]

    df['create_date'] = pd.to_datetime(df['create_date'])
    df['create_time'] = pd.to_datetime(df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['real_logi_time'] = pd.to_datetime(df['real_logi_time'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    df['logistics_interval']=[(td.seconds + td.days * 24 * 3600)/ 3600 for td in (df['real_logi_time']-df['create_time'])]


    df['<1day_purchased'] = np.where(df['purchase_time'] - df['create_time'] < timedelta(days=1), df['qty_purchased'], 0)
    df['<2day_purchased'] = np.where(df['purchase_time'] - df['create_time'] < timedelta(days=2), df['qty_purchased'], 0)
    df['<1day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=1), df['qty_purchased'], 0)
    df['<2day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=2), df['qty_purchased'], 0)
    df['<3day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=3), df['qty_purchased'], 0)
    df['<4day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=4), df['qty_purchased'], 0)
    df['<5day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=5), df['qty_purchased'], 0)
    df['<2day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=2), df['qty_purchased'], 0)
    df['<3day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=3), df['qty_purchased'], 0)
    df['<4day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=4), df['qty_purchased'], 0)
    df['<5day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=5), df['qty_purchased'], 0)
    df['<6day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=6), df['qty_purchased'], 0)
    df['<2day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=2), df['qty_received'], 0)
    df['<3day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=3), df['qty_received'], 0)
    df['<4day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=4), df['qty_received'], 0)
    df['<5day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=5), df['qty_received'], 0)
    df['<6day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=6), df['qty_received'], 0)
    return df
    #1天发货率
def KPI_to_zhang(end,df):
    df=df[(df['create_date']>=end-timedelta(days=35))&(df['create_date']<=end-timedelta(days=6))]
    df['done_real_demand']=np.where(df['state'].isin([4,5,6]),df['qty_real_demand'],0)
    df['done_received'] = np.where(df['state'].isin([4, 5, 6]), df['qty_received'], 0)

    df_sum=pd.DataFrame({'purchase_company_name':'total',
                         'order_rate_t1':df['<1day_purchased'].sum()/df['qty_real_demand'].sum(),
                         'order_rate_t2':df['<2day_purchased'].sum()/ddf['qty_real_demand'].sum(),
                         'storage_rate_t5':df['<5day_storage'].sum()/df['qty_real_demand'].sum(),
                        'done_real_demand':df['done_real_demand'].sum(),
                        'shortage_rate':(df['done_real_demand'].sum()-df['done_received'].sum()/df['done_real_demand'].sum(),
                        'return_rate':df['apply_quantity'].sum()/df['qty_purchased'].sum(),index=[0])

    df = pd.pivot_table(df, values=['<1day_purchased','<2day_purchased','<5day_storage','apply_quantity','qty_purchased','qty_real_demand','done_real_demand','done_received'], index='purchase_company_name', aggfunc=np.sum)
    # print df.head()
    df.reset_index(inplace=True)
    # print df.head()
    df['order_rate_t1'] = df['<1day_purchased'] / df['qty_real_demand']
    df['order_rate_t2'] = df['<2day_purchased'] / df['qty_real_demand']
    df['storage_rate_t5'] = df['<5day_storage'] / df['qty_real_demand']
    df['shortage_rate']=(df['done_real_demand']-df['done_received'])/df['done_real_demand']
    df['return_rate']=df['apply_quantity']/df['qty_purchased']
    df=df[['purchase_company_name','order_rate_t1','order_rate_t2','storage_rate_t5','done_real_demand','shortage_rate','return_rate']]
    df=df.append(df_sum,ignore_index=True)

    return df
def KPI_to_yang(end,df):
    df_one1=order_rate(end,1,1,df)
    df_one2=order_rate(end,2,1,df)
    df_one3=order_rate(end,3,1,df)
    df_one=pd.concat([df_one1, df_one2, df_one3], ignore_index=True)
    df_one = pd.pivot_table(df_one, values=['purchased', 'qty_real_demand'], index='purchase_company_name', aggfunc=np.sum)
    df_one.reset_index(inplace=True)
    df_one['order_rate_avg1']=df_one['purchased'] / df_one['qty_real_demand']#过去3天总的1天发货率

    #两天发货率
    df_two1=order_rate(end,2,2,df)
    df_two2 = order_rate(end,3,2,df)
    df_two3= order_rate(end,4,2,df)
    df_two=pd.concat([df_two1, df_two2, df_two3], ignore_index=True)
    df_two = pd.pivot_table(df_two, values=['purchased', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    df_two.reset_index(inplace=True)
    df_two['order_rate_avg2']=df_two['purchased'] / df_two['qty_real_demand'] #过去3天总的2天发货率

    #五天入库率
    df_wh=warehouse_rate(end,5,5,df)

    #七天缺货率
    df_shortage=shortage_rate(end,14,df)

    #14天退货率
    df_return=return_rate(end,14,df)

    #修改列名以便连接时区分
    df_one1.rename(columns={"purchased": "purchased1", "qty_real_demand": "qty_real_demand1"}, inplace=True)
    df_one.rename(columns={"purchased": "purchased2", "qty_real_demand": "qty_real_demand2"}, inplace=True)
    df_two1.rename(columns={"purchased": "purchased3", "qty_real_demand": "qty_real_demand3"}, inplace=True)
    df_two.rename(columns={"purchased": "purchased4", "qty_real_demand": "qty_real_demand4"}, inplace=True)
    df_wh.rename(columns={"received": "received5", "qty_real_demand": "qty_real_demand5"}, inplace=True)
    df_shortage.rename(columns={"received": "received6", "qty_real_demand": "qty_real_demand6"}, inplace=True)
    df_return.rename(columns={"qty_purchased": "qty_purchased7"}, inplace=True)
    #合并所有指标
    df_all=df_one1.merge(df_one,on='purchase_company_name',how='outer')
    df_all=df_all.merge(df_two1,on='purchase_company_name',how='outer')
    df_all = df_all.merge(df_two, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_wh, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_shortage, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_return, on='purchase_company_name', how='outer')

    df_sum=pd.DataFrame({'purchase_company_name':'total',
                         'order_rate11':df_all['purchased1'].sum()/df_all['qty_real_demand1'].sum(),
                        'order_rate_avg1':df_all['purchased2'].sum()/df_all['qty_real_demand2'].sum(),
                        'order_rate22':df_all['purchased3'].sum()/df_all['qty_real_demand3'].sum(),
                        'order_rate_avg2':df_all['purchased4'].sum()/df_all['qty_real_demand4'].sum(),
                        'warehouse_rate':df_all['received5'].sum()/df_all['qty_real_demand5'].sum(),
                        'qty_real_demand6':df_all['qty_real_demand6'].sum(),
                        'shortage_rate':(df_all['qty_real_demand6'].sum()-df_all['received6'].sum())/df_all['qty_real_demand6'].sum(),
                        'return_rate':df_all['apply_quantity'].sum()/df_all['qty_purchased7'].sum()},index=[0])

    df_part=df_all[['purchase_company_name','order_rate11','order_rate_avg1','order_rate22','order_rate_avg2','warehouse_rate','qty_real_demand6','shortage_rate','return_rate']]

    df_part=df_part.append(df_sum,ignore_index=True)
    df_part=df_part[['purchase_company_name','order_rate11','order_rate_avg1','order_rate22','order_rate_avg2','warehouse_rate','qty_real_demand6','shortage_rate','return_rate']]

    str1=u'1天出货率'+'('+str(end-timedelta(days=1))+')'
    str2=u'1天出货率3日平均'+'('+str(end-timedelta(days=3))+u'至'+str(end-timedelta(days=1))+')'
    str3=u'2天出货率'+'('+str(end-timedelta(days=2))+')'
    str4 = u'2天出货率3日平均' + '(' + str(end - timedelta(days=4)) + u'至' + str(end - timedelta(days=2)) + ')'
    str5=u'5天到库率'+'('+str(end-timedelta(days=5))+')'
    str6=u'已完结需求数'+'(' + str(end - timedelta(days=14)) + u'至' + str(end - timedelta(days=1)) + ')'
    str7=u'14天缺货率'+'(' + str(end - timedelta(days=14)) + u'至' + str(end - timedelta(days=1)) + ')'
    str8=u'14天退货率'+'(' + str(end - timedelta(days=14)) + u'至' + str(end - timedelta(days=1)) + ')'
    df_part.rename(columns={
        'purchase_company_name':u'供应商', 'order_rate11':str1,'order_rate_avg1':str2,'order_rate22':str3, 'order_rate_avg2':str4,
        'warehouse_rate':str5,'qty_real_demand6':str6, 'shortage_rate':str7,'return_rate':str8},inplace=True)

    return df_part
def KPI_to_supply(end,df):
    # 获取供应商和跟单员的对应信息
    df_sup_follow = df[['purchase_company_name', 'follow_user_name', 'category_name']].drop_duplicates('purchase_company_name', keep='last')

    df_one1 = order_rate(end, 1, 1, df)
    df_one2 = order_rate(end, 2, 1, df)
    df_one3 = order_rate(end, 3, 1, df)
    # 两天出货率
    df_two1 = order_rate(end, 2, 2, df)
    df_two2 = order_rate(end, 3, 2, df)

    #过去1天全缺率
    df_short1=shortage_xday(end,2,1,df)
    #过去七天全缺率,只能算到第六天
    df_short7=shortage_xday(end,7,6,df)

    # 修改列名以便连接时区分
    df_one1.rename(columns={"purchased": "purchased1", "qty_real_demand": "qty_real_demand1"}, inplace=True)
    df_one2.rename(columns={"purchased": "purchased2", "qty_real_demand": "qty_real_demand2"}, inplace=True)
    df_one3.rename(columns={"purchased": "purchased3", "qty_real_demand": "qty_real_demand3"}, inplace=True)
    df_two1.rename(columns={"purchased": "purchased4", "qty_real_demand": "qty_real_demand4"}, inplace=True)
    df_two2.rename(columns={"purchased": "purchased5", "qty_real_demand": "qty_real_demand5"}, inplace=True)
    df_short1.rename(columns={ "qty_real_demand": "qty_real_demand6"}, inplace=True)
    df_short7.rename(columns={"qty_real_demand": "qty_real_demand7"}, inplace=True)
    # 合并所有指标
    df_all = df_one1.merge(df_one2, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_one3, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_two1, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_two2, on='purchase_company_name', how='outer')
    df_all=df_all.merge(df_short1,on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_short7, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_sup_follow, on='purchase_company_name', how='left')

    df_all['qty_real_demand'] = df_all['qty_real_demand1'].add(df_all['qty_real_demand2'], fill_value=0).add(
        df_all['qty_real_demand3'], fill_value=0)  # 过去3日总的需求，忽略空值


    df_sum = pd.DataFrame({'purchase_company_name': 'total',
                           'qty_real_demand': df_all['qty_real_demand'].sum(),
                           'order_rate11': df_all['purchased1'].sum() / df_all['qty_real_demand1'].sum(),
                           'order_rate21': df_all['purchased2'].sum() / df_all['qty_real_demand2'].sum(),
                           'order_rate22': df_all['purchased4'].sum() / df_all['qty_real_demand4'].sum(),
                           'order_rate31': df_all['purchased3'].sum() / df_all['qty_real_demand3'].sum(),
                           'order_rate32': df_all['purchased5'].sum() / df_all['qty_real_demand5'].sum(),
                           'all_shortage_rate2': df_all['all_shortage_num2'].sum() / df_all['qty_real_demand6'].sum(),
                           'all_shortage_rate7': df_all['all_shortage_num7'].sum() / df_all['qty_real_demand7'].sum(),
                           'follow_user_name': '',
                           'category_name': ''
                           }, index=[0],
                          columns=['purchase_company_name', 'qty_real_demand', 'order_rate11', 'order_rate21',
                                   'order_rate22', 'order_rate31', 'order_rate32','all_shortage_rate2','all_shortage_rate7', 'follow_user_name', 'category_name'])
    df_part = df_all[['purchase_company_name', 'qty_real_demand', 'order_rate11', 'order_rate21', 'order_rate22', 'order_rate31','order_rate32', 'all_shortage_rate2','all_shortage_rate7','follow_user_name', 'category_name']]
    df_part = df_part.append(df_sum, ignore_index=True)
    df_part = df_part[['purchase_company_name', 'qty_real_demand', 'order_rate11', 'order_rate21', 'order_rate22', 'order_rate31','order_rate32','all_shortage_rate2','all_shortage_rate7','follow_user_name', 'category_name']]

    str1 = u'供应商'
    str2 = u'需求数' + '(' + str(end - timedelta(days=3)) + u'至' + str(end - timedelta(days=1)) + ')'
    str3 = u'1天出货率' + '(' + str(end - timedelta(days=1)) + ')'
    str4 = u'1天出货率' + '(' + str(end - timedelta(days=2)) + ')'
    str5 = u'2天出货率' + '(' + str(end - timedelta(days=2)) + ')'
    str6 = u'1天出货率' + '(' + str(end - timedelta(days=3)) + ')'
    str7 = u'2天出货率' + '(' + str(end - timedelta(days=3)) + ')'
    str8=u'1天全缺率'+'(' + str(end - timedelta(days=2)) + ')'
    str9= u'7天全缺率' + '(' + str(end - timedelta(days=7)) + ')'

    df_part.rename(
        columns={'purchase_company_name': str1, 'qty_real_demand': str2, 'order_rate11': str3, 'order_rate21': str4,
                 'order_rate22': str5, 'order_rate31': str6, 'order_rate32': str7, 'all_shortage_rate2':str8,'all_shortage_rate7':str9,'follow_user_name': u'采购负责人',
                 'category_name': u'品类'}, inplace=True)

    return  df_part
#front向前多少天，interval 间隔时间
def order_rate(end,front,interval,df_all):
    colName='order_rate'+str(front)+str(interval)
    df = df_all[df_all['create_date'] == (end - timedelta(days=front))]
    # print df_one.head()
    df.loc[:, 'purchased'] = np.where(df['purchase_time'] - df['create_time'] <= timedelta(days=interval),df['qty_purchased'], 0)
    #df_sum=pd.DataFrame({'purchased_all':df['purchased'].sum(),})
    df = pd.pivot_table(df, values=['purchased', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    #print df.head()
    df.reset_index(inplace=True)
    #print df.head()
    df[colName] = df['purchased'] / df['qty_real_demand']
    return df
#五天入库率
def warehouse_rate(end,front,interval,df_all):
    df = df_all[df_all['create_date'] == (end - timedelta(days=front))]
    # print df_one.head()
    df.loc[:, 'received'] = np.where(df['storage_time'] - df['create_time'] <= timedelta(days=interval), df['qty_received'], 0)
    df = pd.pivot_table(df, values=['received', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    df.reset_index(inplace=True)
    df['warehouse_rate'] = df['received'] / df['qty_real_demand']
    return df
#x天全缺率 刘璐要求 分母为一天总需求数，分子为状态6的需求数,前移front天，取interval天的全缺数
def shortage_xday(end,front,interval,df_all):
    df_all['all_shortage_num'+str(front)]=np.where(df_all['state']==6,df_all['qty_real_demand'],np.NaN)
    df=df_all[(df_all['create_date']>=(end-timedelta(days=front)))&(df_all['create_date']<(end-timedelta(days=front-interval)))] #取两天前的采购单
    df = pd.pivot_table(df, values=['all_shortage_num'+str(front), 'qty_real_demand'], index='purchase_company_name', aggfunc=np.sum)
    df.reset_index(inplace=True)
    df['all_shortage_rate'+str(front)] = df['all_shortage_num'+str(front)]/df['qty_real_demand']
    return df
#14天缺货率 只选采购单已完结状态(state=4,5,6)
def shortage_rate(end,interval,df_all):
    df = df_all[(df_all['state']==4)|(df_all['state']==5)|(df_all['state']==6)]
    df=df[end-df['create_time']<=timedelta(days=interval)]#取前14天时间段的总和
    # print df_one.head()
    df['received'] = df['qty_received']
    df = pd.pivot_table(df, values=['received', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    df.reset_index(inplace=True)
    df['shortage_rate'] =(df['qty_real_demand']- df['received'])/ df['qty_real_demand']
    return df

#退货率14天
def return_rate(end,interval,df_all):
    df = df_all[end - df_all['create_time'] <= timedelta(days=interval)]
    # print df_one.head()
    df = pd.pivot_table(df, values=['apply_quantity', 'qty_purchased'], index='purchase_company_name',
                            aggfunc=np.sum)
    df.reset_index(inplace=True)
    df['return_rate'] = df['apply_quantity'] / df['qty_purchased']
    return df
#获取采购单的物流时间，入库时间，注意去重保留时间

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()

    conn=db_help.get_idoo_supplier()
    start=datetime.now().date()-timedelta(days=35)
    end=datetime.now().date()
    df=get_po_rawdata(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    # df_yang=KPI_to_yang(end,df)
    # df_supply=KPI_to_supply(end,df)
    df_zhang=KPI_to_zhang(end,df)
    #print df_all.head()
    # path = u'F:\createExcel\采购指标' + str(end) + '.xlsx'
    # writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    # #df.to_excel(writer,sheet_name=str(end)+u'rawdata',index=False,encoding= 'utf8')
    # df_yang.to_excel(writer,sheet_name=str(end)+u'采购指标',index=False,encoding= 'utf8')
    # writer.save()
    # #
    # writer2 = pd.ExcelWriter(u'F:\createExcel\采购发货率' + str(end) + '.xlsx', engine='xlsxwriter', options={'strings_to_urls': False})
    # df_supply.to_excel(writer2, sheet_name=str(end) + u'采购指标', index=False, encoding='utf8')
    # writer2.save()

    writer3 = pd.ExcelWriter(u'F:\createExcel\采购指标30天' + str(end) + '.xlsx', engine='xlsxwriter',options={'strings_to_urls': False})
    df_zhang.to_excel(writer3, sheet_name=str(end) + u'采购指标', index=False, encoding='utf8')
    writer3.save()