#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter


def get_notshipped(end,idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select distinct CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time, po.uuid,sku.sku_no,pro.item_no,pro.title,po.state,
    qty_system_demand,qty_purchased,qty_received,if(length(external_order_id)=17,right(external_order_id,4),'') as 1688_account,purchase_company_name,po.logistics_status,is_cooperative_supplier
    from purchase_order as po
    left join purchase_order_shipping_tickets as post on po.id=post.purchaseorder_id
    left join shipping_ticket as st on post.shippingticket_id=st.id
    left join supply_order as so on po.supply_order_id=so.id
    left join shipping_ticket_supply_orders as stso on so.id=stso.supplyorder_id
    left join shipping_ticket as st2 on stso.shippingticket_id=st2.id
    left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    where state in (1,2,3) and st.tracking_no is null and st2.tracking_no is null and date(CONVERT_TZ(po.create_time,'+00:00','+8:00'))<='{}'

    """.format(end)
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    return df_all,df_all['uuid'].count()
def get_uncollected(end,idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select distinct CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,po.uuid,sku.sku_no,pro.item_no,pro.title,po.state,qty_system_demand,qty_purchased,qty_received,
    if(length(external_order_id)=17,right(external_order_id,4),'') as 1688_account,if(ISNULL(st.tracking_no),st2.tracking_no,st.tracking_no) as tracking_no,purchase_company_name,is_cooperative_supplier,po.logistics_status
    from purchase_order as po
    left join purchase_order_shipping_tickets as post on po.id=post.purchaseorder_id
    left join shipping_ticket as st on post.shippingticket_id=st.id
    left join supply_order as so on po.supply_order_id=so.id
    left join shipping_ticket_supply_orders as stso on so.id=stso.supplyorder_id
    left join shipping_ticket as st2 on stso.shippingticket_id=st2.id
    left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    where state in (1,2,3) and (st.tracking_no is not null or st2.tracking_no is not null) and logistics_status in (0,1) and is_cooperative_supplier=0
    and date(CONVERT_TZ(po.create_time,'+00:00','+8:00'))<='{}'

    """.format(end)
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    return df_all, df_all['uuid'].count()
def get_purchased(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select distinct CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,date(CONVERT_TZ(po.create_time,'+00:00','+8:00')) as create_date,po.uuid,sku.sku_no,pro.item_no,pro.title,po.state,qty_system_demand,qty_purchased,qty_received,
    if(length(external_order_id)=17,right(external_order_id,4),'') as 1688_account,purchase_company_name,is_cooperative_supplier,po.logistics_status
    from purchase_order as po
    left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    left join supply_order as so on po.supply_order_id=so.id
    where state in (1,2,3)  and is_cooperative_supplier=0 
    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all.drop_duplicates('uuid',inplace=True)
    today=datetime.now().date()
    t5=df_all.loc[today-df_all['create_date']>=timedelta(days=5),'uuid'].count()
    t7 = df_all.loc[today - df_all['create_date'] >= timedelta(days=7), 'uuid'].count()
    return df_all,t5,t7

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()

    end1=datetime.now().date()-timedelta(days=4)

    df_notshipped,m=get_notshipped(end1,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    df_uncollected,n=get_uncollected(end1,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    df_purchased,t5,t7 = get_purchased(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)

    df_stat = pd.DataFrame({u'<=T-4无运单':m,u'<=T-4未揽收':n,u'<=T-5已采购':t5,u'<=T-7已采购':t7},index=[datetime.now().date()],
                           columns=[u'<=T-4无运单',u'<=T-4未揽收',u'<=T-5已采购',u'<=T-7已采购'])
    # #print df_all.head()
    path = u'F:\createExcel\无运单未揽收疑难'+str(datetime.now().date())+'.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_notshipped.to_excel(writer,sheet_name=u'无运单',index=False,encoding= 'utf8')
    df_uncollected.to_excel(writer, sheet_name=u'未揽收', index=False, encoding='utf8')
    df_purchased.to_excel(writer, sheet_name=u'已采购', index=False, encoding='utf8')
    df_total.to_excel(writer, sheet_name=u'<=T-1疑难', index=False, encoding='utf8')

    writer.save()