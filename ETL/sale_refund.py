#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter

def get_refund_info(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    select  CONVERT_TZ(so.create_at,'+00:00','+8:00') as create_at,so.order_name,origin_total,amount_total,shipping_country,if(lower(shipping_country)='india','India','Other') as country,
    CONVERT_TZ(socs.create_time,'+00:00','+8:00') as cod_createtime,status,if(ISNULL(status),'pre-paid','COD') as payment,CONVERT_TZ(sor.create_time,'+00:00','+8:00') as pre_createtime,
    total_refund,sor.state,reason_type,if(ISNULL(socs.create_time),sor.create_time,socs.create_time) as refund_createtime
    from sale_order as so 
    left join sale_order_cod_status as socs on so.order_name=socs.order_name
    left join sale_order_refund as sor on so.order_name=sor.order_name
    
    where CONVERT_TZ(so.create_at,'+00:00','+8:00') between '{}' and '{}'
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    #status是cod状态，returntype是非cod退款原因

    df['is_return_goods']=np.where((df['status']=='reject')|(df['reason_type']=='return'),'Y','N')
    df['is_refund']=np.where((df['status']=='canceled')|((pd.notnull(df['reason_type']))&(df['reason_type']!='return')),'Y','N')
    df['refund_return_total']=np.where((df['status']=='canceled')|(df['status']=='reject'),df['origin_total'],df['total_refund'])
    df['only_return_amount']=np.where( df['is_return_goods']=='Y', df['refund_return_total'],np.NaN)
    df['only_refund_amount']=np.where( df['is_refund']=='Y', df['refund_return_total'],np.NaN)

    df_pivot=pd.pivot_table(df,values=['only_return_amount','only_refund_amount'],index=['country','payment'],aggfunc=np.sum)
    df_pivot.reset_index(inplace=True)
    return df_pivot
if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()

    start = '2017-11-01'
    end = '2017-11-05'
    start = datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.strptime(end, "%Y-%m-%d").date()

    df=get_refund_info(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)

    path = r'F:\createExcel\refund_info' + str(end) + '.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df.to_excel(writer, sheet_name=str(end) + u'采购指标', index=False, encoding='utf8')
    writer.save()