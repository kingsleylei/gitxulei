#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help



def get_info_sor(idoo_conn, idoo_xs_conn,idoo_hn_conn,start,end):
    
    sql = '''
        select distinct sor.order_name,so.shipping_country,
            sor.total_refund,sor.channel,sor.reason_type,
            DATE(CONVERT_TZ(sor.create_time,'+00:00','+08:00')) as create_at
        from 
            sale_order_refund as sor 
            join sale_order as so on so.order_name=sor.order_name
        where 
            DATE(CONVERT_TZ(sor.create_time,'+00:00','+08:00')) >= '{}'
            and DATE(CONVERT_TZ(sor.create_time,'+00:00','+08:00')) <= '{}'
        '''.format(start,end)
    df_yh = pd.read_sql(sql, con=idoo_conn)    
    df_xs = pd.read_sql(sql, con=idoo_xs_conn)
    df_hn = pd.read_sql(sql, con=idoo_hn_conn)
    return df_yh,df_xs,df_hn



if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn = db_help.get_new_idoo_hn_conn()


    start_date = '2017-10-27'                                          #使用查询当天的时间
    end_date   = '2017-10-29'                                          #使用查询当天的时间
    start = datetime.datetime.strptime(start_date,"%Y-%m-%d").date()        #采购单生成计算时间
    end = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()
    
    df_yh_sor,df_xs_sor,df_hn_sor = get_info_sor( idoo_yh_conn, idoo_xs_conn,idoo_hn_conn,start,end)
 
    
    
    path = 'F:\\' + start_date + u'至'+ end_date + u'退款明细.xlsx'
    writer = pd.ExcelWriter(path)
    df_yh_sor.to_excel(writer,u'余杭',index = False,encoding= 'utf8')
    df_xs_sor.to_excel(writer,u'萧山',index = False,encoding= 'utf8')
    df_hn_sor.to_excel(writer,u'心怡',index = False,encoding= 'utf8')
    writer.save()

    

