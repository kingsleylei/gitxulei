#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlsxwriter


def get_sale_info(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select item_no,product_qty as sale_qty,origin_qty
    from sale_order_line
    where item_no is NOT NULL
    

    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_sale = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_sale=pd.pivot_table(df_sale,index='item_no',values=['sale_qty','origin_qty'],aggfunc=np.sum)
    return df_sale

def get_return_apply(conn):
    sql="""
    SELECT product_no,return_reason,quantity
    FROM return_apply
    where CONVERT_TZ(create_at,'+00:00','+8:00') <'2017-10-28' and flag=0
    """
    df=pd.read_sql(sql,conn)
    df['return_reason']=df['return_reason'].str.lower()
    df.loc[~((df['return_reason']=='too small')|(df['return_reason']=='too large')|(df['return_reason']=='bad material')),'return_reason']='other reason'
    df_reason=pd.crosstab(df.product_no,df.return_reason)
    df_quantity=pd.pivot_table(df,index='product_no',values='quantity',aggfunc=np.sum)
    df_return=df_reason.merge(df_quantity,left_index=True,right_index=True,how='inner')
    #print df_return
    return df_return


if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn = db_help.get_new_idoo_hn_conn()
    app_conn=db_help.get_apporder_conn()

    df_reason=get_return_apply(app_conn)
    df_sale=get_sale_info(idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)

    df_end=df_reason.merge(df_sale,left_index=True,right_index=True,how='inner')
    df_end['return_rate']=df_end['quantity']/df_end['sale_qty']
    df_end['return_rate2']=df_end['quantity']/df_end['origin_qty']
    #print df_end
    # start = '2017-10-16'  # 16号0点开始
    # end = '2017-10-23'  # 注意结束时间 23号0点结束
    # start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
    # end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
   # df_all = get_sale_info(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    # df_all.to_excel('F:\createExcel\get_po_info.xlsx',index=False)


    path = r'F:\createExcel\return_apply3.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_end.to_excel(writer, encoding='utf8')
    writer.save()