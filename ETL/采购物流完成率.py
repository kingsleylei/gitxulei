#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlsxwriter

def get_po_info(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn,supplier_conn):
    sql="""
    SELECT po.uuid,CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(po.purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(st.create_time,'+00:00','+8:00') as logistics_createtime,CONVERT_TZ(log.create_time,'+00:00','+8:00')as person_createTime,
    CONVERT_TZ(po.signed_time,'+00:00','+8:00') as signed_time,CONVERT_TZ(po.complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(storage_time,'+00:00','+8:00') as storage_time,is_cooperative_supplier,po.source_type,pro.category_name,state,prev_state
    from
    purchase_order as po left join supply_order as supo on supply_order_id=supo.id
    left join shipping_ticket_supply_orders as stso on supo.id=stso.supplyorder_id
    left join shipping_ticket as st on stso.shippingticket_id=st.id
    left join
    (select content,object_id,create_time
    from log_logmessage 
    where content like '%人工物流单号%') as log on po.id =log.object_id
    left join
    (select object_id,min(create_time) as storage_time
    from log_logmessage  
    where content like '%入库%'
    group by object_id
    ) as logl on po.id=logl.object_id
    left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    where po.create_time between '{}' and '{}' and po.source_type in (0,2) 
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)

    #df = df.replace(pd.NaT, '')
    df['create_time']=pd.to_datetime( df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['logistics_createtime'] = pd.to_datetime(df['logistics_createtime'])
    df['person_createTime'] = pd.to_datetime(df['person_createTime'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])

    #df.to_excel('F:\createExcel\original_data2.xlsx', index=False)
    #print df.head(10)
    df.loc[df['state']==1,'storage_time']=pd.NaT
    df['real_logi_time']=np.where(pd.isnull(df['logistics_createtime']),df['person_createTime'],df['logistics_createtime'])
    df['signed_time'] = np.where(pd.isnull(df['signed_time']), df['storage_time'], df['signed_time'])

    #print df.dtypes
    df['real_logi_time']=np.where((pd.notnull(df['purchase_time']))&(pd.notnull(df['signed_time']))&(pd.isnull(df['real_logi_time'])),df['purchase_time']+(df['signed_time']-df['purchase_time'])/2,df['real_logi_time'])
    df['real_logi_time']=np.where(df['real_logi_time']<df['purchase_time'],df['purchase_time'],df['real_logi_time'])
    df[['create_time','purchase_time', 'real_logi_time', 'signed_time', 'storage_time']] = df[['create_time', 'purchase_time', 'real_logi_time', 'signed_time', 'storage_time']].values.astype('<M8[D]')
    df_rawdata=df[['uuid','create_time','purchase_time','real_logi_time','signed_time','storage_time','is_cooperative_supplier','source_type','category_name','state','prev_state','warehouse']]

    #print df_rawdata.head(10)
    #df_all.loc[df_all['qty_system_demand']==0,'return_quantity']=0

    df[[ 'create_time', 'purchase_time', 'real_logi_time', 'signed_time', 'storage_time']]=df[[ 'create_time', 'purchase_time', 'real_logi_time', 'signed_time', 'storage_time']].values.astype('<M8[D]')
    #非合作供应商系统单
    df_one=df[(df['is_cooperative_supplier']==0)&(df['source_type']==0)]


    #非合作供应商补单
    df_two = df[(df['is_cooperative_supplier'] == 0) & (df['source_type'] == 2)]

    #合作供应商系统单
    df_thr = df[(df['is_cooperative_supplier'] == 1) & (df['source_type'] == 0)]
    return df_rawdata

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()
    start='2017-11-01'
    end='2017-11-02'
    start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
    df_all=get_po_info(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn,conn)

    path = r'F:\createExcel\采购物流完成率' + str(start) + '-' + str(end) + '.xlsx'
    path = unicode(path, "utf-8")
    df_all.to_excel(path, index=False)



    # path = r'F:\createExcel\get_po_KPI'+str(start)+'-'+str(end)+'.xlsx'
    # writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    # df_all.to_excel(writer, index=False, encoding='utf8')
    # writer.save()