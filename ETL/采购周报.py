#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter

pd.options.mode.chained_assignment = None  # default='warn'
#获取物流时间和入科时间
def get_storage_time(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    SELECT po.uuid,CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(po.purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(st.create_time,'+00:00','+8:00') as person_logi_time, CONVERT_TZ(st2.create_time,'+00:00','+8:00') as sys_logi_time,CONVERT_TZ(po.signed_time,'+00:00','+8:00') as signed_time,
    CONVERT_TZ(po.complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(storage_time,'+00:00','+8:00') as storage_time,state,is_cooperative_supplier
    from
    purchase_order as po 
    left join purchase_order_shipping_tickets as post on po.id=post.purchaseorder_id
    left join shipping_ticket as st on post.shippingticket_id=st.id
    left join supply_order as so on po.supply_order_id=so.id
    left join shipping_ticket_supply_orders as stso on so.id=stso.supplyorder_id
    left join shipping_ticket as st2 on stso.shippingticket_id=st2.id
    left join
    (select object_id,max(create_time) as storage_time
    from log_logmessage  
    where content like '%入库%'
    group by object_id
    ) as logl on po.id=logl.object_id
    where po.create_time between '{}' and '{}' and po.source_type in (0,2) 
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    #保留时间物流时间较早的
    df.drop_duplicates('uuid',inplace=True)

    df['create_time'] = pd.to_datetime(df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['person_logi_time'] = pd.to_datetime(df['person_logi_time'])
    df['sys_logi_time'] = pd.to_datetime(df['sys_logi_time'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])
    #物流单号生成时间
    df['real_logi_time']=np.where((pd.notnull(df['person_logi_time']))&(pd.notnull(df['sys_logi_time'])),np.where(df['person_logi_time']<df['sys_logi_time'],df['person_logi_time'],df['sys_logi_time']),
                                  np.where(pd.isnull(df['person_logi_time']),df['sys_logi_time'],df['person_logi_time']))
    #未采购 已采购 全部缺货情况下没有入库时间
    df.loc[(df['state'] == 0)|(df['state'] == 1)|(df['state'] == 6),'storage_time'] = pd.NaT
    #无签收时间取入库时间
    df['signed_time'] = np.where(pd.isnull(df['signed_time']), df['storage_time'], df['signed_time'])

    df['real_logi_time']=np.where(pd.isnull(df['purchase_time']),pd.NaT,df['real_logi_time'])
    # KA的发货时间：有发货的，定义为创建时间采购时间发货
    df['real_logi_time'] = np.where((df['is_cooperative_supplier'] == 1)&(pd.notnull(df['signed_time']))& (pd.isnull(df['real_logi_time'])),df['purchase_time'],df['real_logi_time'])

    #print df['real_logi_time'].dtype
    #非KA无物流时间，取签收时间和购买时间的中值
    df['real_logi_time'] = np.where((df['is_cooperative_supplier'] == 0)&(pd.notnull(df['purchase_time'])) & (pd.notnull(df['signed_time'])) & (pd.isnull(df['real_logi_time'])),df['purchase_time'] + (df['signed_time'] - df['purchase_time']) / 2, df['real_logi_time'])
    #经测试，物流时间有可能早于采购时间，只好取采购时间
    df['real_logi_time'] = np.where(df['real_logi_time'] < df['purchase_time'], df['purchase_time'],df['real_logi_time'])


    #df.to_excel(u'F:\createExcel\采购完成率test2原来原来.xlsx', index=False, encoding='utf8')

    df=df[['uuid','real_logi_time','signed_time','storage_time']]

    return df
def get_po_rawdata(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    select date(CONVERT_TZ(po.create_time,'+00:00','+8:00')) as create_date,CONCAT('WK',WEEK(date(CONVERT_TZ(po.create_time,'+00:00','+8:00')),1)+1) as week,po.uuid,qty_system_demand,qty_purchase_demand,qty_purchased,qty_received,supply_qyt_sys_demand,supply_qyt_purchased,
    purchase_price,qty_purchased*purchase_price as real_purchase_amount, CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(signed_time,'+00:00','+8:00') as signed_time_ch,timestampdiff(SECOND,po.create_time,purchase_time)/3600 as order_interval,
    purchase_company_name,is_cooperative_supplier,apply_quantity,gr.status,sku_no,item_no,source_type,po.state,po.prev_state,po.follow_user_name,category_name
    from
    (select * from
    (select *
    from purchase_order
    where CONVERT_TZ(create_time,'+00:00','+8:00') between '{}' and '{}' and source_type in (0,2) and state !=7) as b
    left join 
    (select source_purchase_order_id as sp_order_id,sum(qty_system_demand) as supply_qyt_sys_demand,sum(qty_purchased) as supply_qyt_purchased
    from purchase_order group by source_purchase_order_id
    ) as c on b.id=c.sp_order_id) as po 
    left join 	
    goods_return as gr on po.id=gr.purchase_order_id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = 'YH' #为了做排序,排在xs,xy前面

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'xs'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'xy'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)

   # df_all['qty_real_demand']=np.where(df_all['qty_system_demand']!=0,df_all['qty_purchased'],max(df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_all['qty_real_demand'] = np.where(df_all['qty_system_demand']!=0,np.where(df_all['qty_system_demand']>df_all['qty_purchased'],df_all['qty_system_demand'],df_all['qty_purchased']),
                                         np.where(df_all['supply_qyt_sys_demand']>df_all['supply_qyt_purchased'],df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_temp=df_all.pop('qty_real_demand')
    df_all.insert(8,'qty_real_demand',df_temp)
    #取得完整的采购时间轴
    # df_storage=get_storage_time(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn)
    # df=df_all.merge(df_storage,on='uuid',how='inner')
    df = df_all.drop_duplicates('uuid', keep='last')

    del df['signed_time_ch']
    df.loc[df['status']==3,'apply_quantity']=0
    df['purchase_amount'] = df['purchase_price'] * df['qty_real_demand']
    df.fillna(0,inplace=True)
    #df=df[(df['is_cooperative_supplier']==1)&(df['source_type']==0)]

    # df['create_date'] = pd.to_datetime(df['create_date'])
    # df['create_time'] = pd.to_datetime(df['create_time'])
    # df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    # #df['real_logi_time'] = pd.to_datetime(df['real_logi_time'])
    # df['signed_time'] = pd.to_datetime(df['signed_time'])
    # df['storage_time'] = pd.to_datetime(df['storage_time'])
    # df['complete_time'] = pd.to_datetime(df['complete_time'])
    # df['logistics_interval']=[(td.seconds + td.days * 24 * 3600)/ 3600 for td in (df['real_logi_time']-df['create_time'])]

    return df
def KA_week_report(df):
    df = df[df['is_cooperative_supplier'] == 1]
    df['shortage_6']=np.where(df['state']==6,1,0)#缺货批次数
    #为了算缺货率，只算已完结的状态，这样不会随时间变化
    df['vain_received']=np.where(df['state'].isin([4,5,6]),'qty_received',0)
    df['vain_real_demand'] = np.where(df['state'].isin([4, 5, 6]), 'qty_real_demand', 0)
    #形成列层次化索引
    df_pivot=pd.pivot_table(df,values=['uuid','qty_real_demand','purchase_amount','shortage_6'],index='week',columns='warehouse',
                            aggfunc={'uuid':'count','qty_real_demand':'sum','purchase_amount':'sum','shortage_6':'sum'})
    df_pivot['uuid','all']= df_pivot['uuid','YH']+df_pivot['uuid','xs']+df_pivot['uuid','xy']
    df_pivot['qty_real_demand', 'all'] = df_pivot['qty_real_demand', 'YH'] + df_pivot['qty_real_demand', 'xs'] + df_pivot['qty_real_demand', 'xy']
    df_pivot['purchase_amount', 'all'] = df_pivot['purchase_amount', 'YH'] + df_pivot['purchase_amount', 'xs'] + df_pivot['purchase_amount', 'xy']
    df_pivot['shortage_6', 'all'] = df_pivot['shortage_6', 'YH'] + df_pivot['shortage_6', 'xs'] +df_pivot['shortage_6', 'xy']
    df_pivot.sort_index(axis=1, level=0, inplace=True)
    df_pivot.reset_index(inplace=True)
    #df=df.groupby('week').agg({'uuid':np.count,})
    df_grouped=df.groupby('week',as_index=False).agg({'sku_no':lambda arr:arr.drop_duplicates().size,'vain_received':'sum','vain_real_demand':'sum'})
    df_grouped['shortage_rate']= (df_grouped['vain_real_demand']- df_grouped['vain_received'])/ df_grouped['vain_real_demand']

    #print df_pivot.head()
    #1天发货率
def allSupplies_week_report(df):

    df_grouped=df.groupby('week',as_index=False).agg({'purchase_company_name':lambda arr:arr.drop_duplicates().size,'item_no':lambda arr:arr.drop_duplicates().size,
                                       'sku_no': lambda arr: arr.drop_duplicates().size,'uuid':'count','purchase_amount':'sum','qty_real_demand':'sum'})
    df_grouped['avg_purchased_price']=df_grouped['purchase_amount']/df_grouped['qty_real_demand']
    # df_sku.rename(columns={'sku_no': 'top100_skuNum'}, inplace=True)
    #按周数和供应商分组
    df_g2=df.groupby(['week','purchase_company_name'],as_index=False).agg({'purchase_amount':'sum'})#为了得到top1%，%20，%50的商家采购总额,先按星期和供应商分组
    df_top100=get_topNp_company(df_g2,100)
    df_top1p=get_topNp_company(df_g2,0.01)
    df_top20p = get_topNp_company(df_g2, 0.2)
    df_top50p = get_topNp_company(df_g2, 0.5)
    df_sku=get_topN_skuNum(df,df_g2)
    data_frames=[df_grouped,df_top100,df_sku,df_top1p,df_top20p,df_top20p,df_top50p]
    df_merged = reduce(lambda left, right: pd.merge(left, right, on='week',how='left'), data_frames)
    df_merged['top100_amountPer']= df_merged['top100amount']/ df_merged['purchase_amount']
    df_merged['top100_skuPer'] = df_merged['top100_skuNum'] / df_merged['sku_no']
    df_merged['top0.01per'] = df_merged['top0.01amount'] / df_merged['purchase_amount']
    df_merged['top0.2per'] = df_merged['top0.2amount'] / df_merged['purchase_amount']
    df_merged['top0.5per'] = df_merged['top0.5amount'] / df_merged['purchase_amount']
    df_last=df_merged[['week','purchase_company_name','item_no', 'sku_no','uuid','purchase_amount','qty_real_demand','avg_purchased_price','top100amount',
                       'top100_amountPer','top100_skuNum' ,'top100_skuPer','top0.01per','top0.2per','top0.5per']]
    #df_g3=df.groupby(['week','sku_no']).apply(top)
    print df_last.head()
#np为top百分比或者topN
def get_topNp_company(df,np):
    df_all=pd.DataFrame()
    for week,group in df.groupby('week'):
        group.fillna(0,inplace=True) #会出现Nan值，填充避免排序出错
        group.sort_values(by='purchase_amount',inplace=True)
        #print group.head()
        if np<1:
            n=int(group['purchase_company_name'].count()*np)
        else:
            n=np
        pur_sum=group[-n:]['purchase_amount'].sum()
        df_temp=pd.DataFrame({'week':week,'top'+str(np)+'amount':pur_sum},index=[0])
        df_all=df_all.append(df_temp,ignore_index=True)

    return df_all
def get_topN_skuNum(df_raw,df_g,n=100):
    df_topCompany=df_g.groupby('week').apply(lambda df:df.sort_values(by='purchase_amount')[-n:])
    print df_topCompany.head()
    df_topCompany.reset_index(inplace=True)
    df_temp=df_raw.merge(df_topCompany,on=['week','purchase_company_name'],how='inner') #得到只包含topN商家的采购信息
    df_sku=df_temp.groupby('week',as_index=False).agg({'sku_no':lambda arr:arr.drop_duplicates().size})
    df_sku.rename(columns={'sku_no':'top100_skuNum'},inplace=True)
    return df_sku

# def top(df,n=100,sort_col='purchase_amount'):
#     df.sort_values(by=sort_col)[-n:]#降序排列
#     return df[sort_col].cumsum()[-1:]
def KPI_to_yang(end,df):
    df_one1=order_rate(end,1,1,df)
    df_one2=order_rate(end,2,1,df)
    df_one3=order_rate(end,3,1,df)
    df_one=pd.concat([df_one1, df_one2, df_one3], ignore_index=True)
    df_one = pd.pivot_table(df_one, values=['purchased', 'qty_real_demand'], index='purchase_company_name', aggfunc=np.sum)
    df_one.reset_index(inplace=True)
    df_one['order_rate_avg1']=df_one['purchased'] / df_one['qty_real_demand']#过去3天总的1天发货率

    #两天发货率
    df_two1=order_rate(end,2,2,df)
    df_two2 = order_rate(end,3,2,df)
    df_two3= order_rate(end,4,2,df)
    df_two=pd.concat([df_two1, df_two2, df_two3], ignore_index=True)
    df_two = pd.pivot_table(df_two, values=['purchased', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    df_two.reset_index(inplace=True)
    df_two['order_rate_avg2']=df_two['purchased'] / df_two['qty_real_demand'] #过去3天总的2天发货率

    #五天入库率
    df_wh=warehouse_rate(end,5,5,df)

    #七天缺货率
    df_shortage=shortage_rate(end,14,df)

    #14天退货率
    df_return=return_rate(end,14,df)

    #修改列名以便连接时区分
    df_one1.rename(columns={"purchased": "purchased1", "qty_real_demand": "qty_real_demand1"}, inplace=True)
    df_one.rename(columns={"purchased": "purchased2", "qty_real_demand": "qty_real_demand2"}, inplace=True)
    df_two1.rename(columns={"purchased": "purchased3", "qty_real_demand": "qty_real_demand3"}, inplace=True)
    df_two.rename(columns={"purchased": "purchased4", "qty_real_demand": "qty_real_demand4"}, inplace=True)
    df_wh.rename(columns={"received": "received5", "qty_real_demand": "qty_real_demand5"}, inplace=True)
    df_shortage.rename(columns={"received": "received6", "qty_real_demand": "qty_real_demand6"}, inplace=True)
    df_return.rename(columns={"qty_purchased": "qty_purchased7"}, inplace=True)
    #合并所有指标
    df_all=df_one1.merge(df_one,on='purchase_company_name',how='outer')
    df_all=df_all.merge(df_two1,on='purchase_company_name',how='outer')
    df_all = df_all.merge(df_two, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_wh, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_shortage, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_return, on='purchase_company_name', how='outer')

    df_sum=pd.DataFrame({'purchase_company_name':'total',
                         'order_rate11':df_all['purchased1'].sum()/df_all['qty_real_demand1'].sum(),
                        'order_rate_avg1':df_all['purchased2'].sum()/df_all['qty_real_demand2'].sum(),
                        'order_rate22':df_all['purchased3'].sum()/df_all['qty_real_demand3'].sum(),
                        'order_rate_avg2':df_all['purchased4'].sum()/df_all['qty_real_demand4'].sum(),
                        'warehouse_rate':df_all['received5'].sum()/df_all['qty_real_demand5'].sum(),
                        'qty_real_demand6':df_all['qty_real_demand6'].sum(),
                        'shortage_rate':(df_all['qty_real_demand6'].sum()-df_all['received6'].sum())/df_all['qty_real_demand6'].sum(),
                        'return_rate':df_all['apply_quantity'].sum()/df_all['qty_purchased7'].sum()},index=[0])

    df_part=df_all[['purchase_company_name','order_rate11','order_rate_avg1','order_rate22','order_rate_avg2','warehouse_rate','qty_real_demand6','shortage_rate','return_rate']]

    df_part=df_part.append(df_sum,ignore_index=True)
    df_part=df_part[['purchase_company_name','order_rate11','order_rate_avg1','order_rate22','order_rate_avg2','warehouse_rate','qty_real_demand6','shortage_rate','return_rate']]

    str1=u'1天出货率'+'('+str(end-timedelta(days=1))+')'
    str2=u'1天出货率3日平均'+'('+str(end-timedelta(days=3))+u'至'+str(end-timedelta(days=1))+')'
    str3=u'2天出货率'+'('+str(end-timedelta(days=2))+')'
    str4 = u'2天出货率3日平均' + '(' + str(end - timedelta(days=4)) + u'至' + str(end - timedelta(days=2)) + ')'
    str5=u'5天到库率'+'('+str(end-timedelta(days=5))+')'
    str6=u'已完结需求数'+'(' + str(end - timedelta(days=14)) + u'至' + str(end - timedelta(days=1)) + ')'
    str7=u'14天缺货率'+'(' + str(end - timedelta(days=14)) + u'至' + str(end - timedelta(days=1)) + ')'
    str8=u'14天退货率'+'(' + str(end - timedelta(days=14)) + u'至' + str(end - timedelta(days=1)) + ')'
    df_part.rename(columns={
        'purchase_company_name':u'供应商', 'order_rate11':str1,'order_rate_avg1':str2,'order_rate22':str3, 'order_rate_avg2':str4,
        'warehouse_rate':str5,'qty_real_demand6':str6, 'shortage_rate':str7,'return_rate':str8},inplace=True)

    return df_part
def KPI_to_supply(end,df):
    # 获取供应商和跟单员的对应信息
    df_sup_follow = df[['purchase_company_name', 'follow_user_name', 'category_name']].drop_duplicates('purchase_company_name', keep='last')

    df_one1 = order_rate(end, 1, 1, df)
    df_one2 = order_rate(end, 2, 1, df)
    df_one3 = order_rate(end, 3, 1, df)
    # 两天出货率
    df_two1 = order_rate(end, 2, 2, df)
    df_two2 = order_rate(end, 3, 2, df)
    # 修改列名以便连接时区分
    df_one1.rename(columns={"purchased": "purchased1", "qty_real_demand": "qty_real_demand1"}, inplace=True)
    df_one2.rename(columns={"purchased": "purchased2", "qty_real_demand": "qty_real_demand2"}, inplace=True)
    df_one3.rename(columns={"purchased": "purchased3", "qty_real_demand": "qty_real_demand3"}, inplace=True)
    df_two1.rename(columns={"purchased": "purchased4", "qty_real_demand": "qty_real_demand4"}, inplace=True)
    df_two2.rename(columns={"purchased": "purchased5", "qty_real_demand": "qty_real_demand5"}, inplace=True)
    # 合并所有指标
    df_all = df_one1.merge(df_one2, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_one3, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_two1, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_two2, on='purchase_company_name', how='outer')
    df_all = df_all.merge(df_sup_follow, on='purchase_company_name', how='left')

    df_all['qty_real_demand'] = df_all['qty_real_demand1'].add(df_all['qty_real_demand2'], fill_value=0).add(
        df_all['qty_real_demand3'], fill_value=0)  # 过去3日总的需求，忽略空值


    df_sum = pd.DataFrame({'purchase_company_name': 'total',
                           'qty_real_demand': df_all['qty_real_demand'].sum(),
                           'order_rate11': df_all['purchased1'].sum() / df_all['qty_real_demand1'].sum(),
                           'order_rate21': df_all['purchased2'].sum() / df_all['qty_real_demand2'].sum(),
                           'order_rate22': df_all['purchased4'].sum() / df_all['qty_real_demand4'].sum(),
                           'order_rate31': df_all['purchased3'].sum() / df_all['qty_real_demand3'].sum(),
                           'order_rate32': df_all['purchased5'].sum() / df_all['qty_real_demand5'].sum(),
                           'follow_user_name': '',
                           'category_name': ''
                           }, index=[0],
                          columns=['purchase_company_name', 'qty_real_demand', 'order_rate11', 'order_rate21',
                                   'order_rate22', 'order_rate31', 'order_rate32', 'follow_user_name', 'category_name'])
    df_part = df_all[['purchase_company_name', 'qty_real_demand', 'order_rate11', 'order_rate21', 'order_rate22', 'order_rate31','order_rate32', 'follow_user_name', 'category_name']]
    df_part = df_part.append(df_sum, ignore_index=True)
    df_part = df_part[['purchase_company_name', 'qty_real_demand', 'order_rate11', 'order_rate21', 'order_rate22', 'order_rate31','order_rate32', 'follow_user_name', 'category_name']]

    str1 = u'供应商'
    str2 = u'需求数' + '(' + str(end - timedelta(days=3)) + u'至' + str(end - timedelta(days=1)) + ')'
    str3 = u'1天出货率' + '(' + str(end - timedelta(days=1)) + ')'
    str4 = u'1天出货率' + '(' + str(end - timedelta(days=2)) + ')'
    str5 = u'2天出货率' + '(' + str(end - timedelta(days=2)) + ')'
    str6 = u'1天出货率' + '(' + str(end - timedelta(days=3)) + ')'
    str7 = u'2天出货率' + '(' + str(end - timedelta(days=3)) + ')'
    str8 = u'采购负责人'
    str9 = u'品类'

    df_part.rename(
        columns={'purchase_company_name': str1, 'qty_real_demand': str2, 'order_rate11': str3, 'order_rate21': str4,
                 'order_rate22': str5, 'order_rate31': str6, 'order_rate32': str7, 'follow_user_name': str8,
                 'category_name': str9}, inplace=True)

    return  df_part
#front向前多少天，interval 间隔时间
def order_rate(end,front,interval,df_all):
    colName='order_rate'+str(front)+str(interval)
    df = df_all[df_all['create_date'] == (end - timedelta(days=front))]
    # print df_one.head()
    df.loc[:, 'purchased'] = np.where(df['purchase_time'] - df['create_time'] <= timedelta(days=interval),df['qty_purchased'], 0)
    #df_sum=pd.DataFrame({'purchased_all':df['purchased'].sum(),})
    df = pd.pivot_table(df, values=['purchased', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    #print df.head()
    df.reset_index(inplace=True)
    #print df.head()
    df[colName] = df['purchased'] / df['qty_real_demand']
    return df
#五天入库率
def warehouse_rate(end,front,interval,df_all):
    df = df_all[df_all['create_date'] == (end - timedelta(days=front))]
    # print df_one.head()
    df.loc[:, 'received'] = np.where(df['storage_time'] - df['create_time'] <= timedelta(days=interval), df['qty_received'], 0)
    df = pd.pivot_table(df, values=['received', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    df.reset_index(inplace=True)
    df['warehouse_rate'] = df['received'] / df['qty_real_demand']
    return df
#14天缺货率 只选采购单已完结状态(state=4,5,6)
def shortage_rate(end,interval,df_all):
    df = df_all[(df_all['state']==4)|(df_all['state']==5)|(df_all['state']==6)]
    df=df[end-df['create_time']<=timedelta(days=interval)]#取前14天时间段的总和
    # print df_one.head()
    df['received'] = df['qty_received']
    df = pd.pivot_table(df, values=['received', 'qty_real_demand'], index='purchase_company_name',aggfunc=np.sum)
    df.reset_index(inplace=True)
    df['shortage_rate'] =(df['qty_real_demand']- df['received'])/ df['qty_real_demand']
    return df

#退货率14天
def return_rate(end,interval,df_all):
    df = df_all[end - df_all['create_time'] <= timedelta(days=interval)]
    # print df_one.head()
    df = pd.pivot_table(df, values=['apply_quantity', 'qty_purchased'], index='purchase_company_name',
                            aggfunc=np.sum)
    df.reset_index(inplace=True)
    df['return_rate'] = df['apply_quantity'] / df['qty_purchased']
    return df
#获取采购单的物流时间，入库时间，注意去重保留时间

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()
    start = '2017-11-13'
    end = '2017-11-27'
    start = datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.strptime(end, "%Y-%m-%d").date()

    df=get_po_rawdata(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
   # KA_week_report(df)
    allSupplies_week_report(df)
    # df_yang=KPI_to_yang(end,df)
    # df_supply=KPI_to_supply(end,df)54

    #print df_all.head()
    # path = u'F:\createExcel\采购指标' + str(end) + '.xlsx'
    # writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    # df_yang.to_excel(writer,sheet_name=str(end)+u'采购指标',index=False,encoding= 'utf8')
    # writer.save()
    #
    # writer2 = pd.ExcelWriter(u'F:\createExcel\采购发货率' + str(end) + '.xlsx', engine='xlsxwriter', options={'strings_to_urls': False})
    # df_supply.to_excel(writer2, sheet_name=str(end) + u'采购指标', index=False, encoding='utf8')
    # writer2.save()

    # path = u'F:\createExcel\采购指标rawdata' +str(start)+'--' +str(end) + '.xlsx'
    # writer3 = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    # df.to_excel(writer3, sheet_name=str(end) + u'采购指标rawdata', index=False, encoding='utf8')
    # writer3.save()