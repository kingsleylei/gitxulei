#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter

pd.options.mode.chained_assignment = None  # default='warn'
def get_po_info(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    select date_format(CONVERT_TZ(po.create_time,'+00:00','+8:00'),"%Y-%m-%d") as create_date,po.uuid,qty_system_demand,qty_purchase_demand,qty_purchased,qty_received,supply_qyt_sys_demand,supply_qyt_purchased,
    purchase_price,qty_purchased*purchase_price as purchase_amount, CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(signed_time,'+00:00','+8:00') as signed_time_ch,timestampdiff(SECOND,po.create_time,purchase_time)/3600 as order_interval,
    timestampdiff(SECOND,purchase_time,signed_time)/3600 as logistics_interval,purchase_company_name,is_cooperative_supplier,apply_quantity,gr.status,sku_no,item_no,source_type,po.state,po.prev_state
    from
    (select * from
    (select *
    from purchase_order
    where CONVERT_TZ(create_time,'+00:00','+8:00') between '{}' and '{}' and source_type in (0,2)) as b
    left join 
    (select source_purchase_order_id as sp_order_id,sum(qty_system_demand) as supply_qyt_sys_demand,sum(qty_purchased) as supply_qyt_purchased
    from purchase_order group by source_purchase_order_id
    ) as c on b.id=c.sp_order_id) as po 
    left join 	
    goods_return as gr on po.id=gr.purchase_order_id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
   # df_all['qty_real_demand']=np.where(df_all['qty_system_demand']!=0,df_all['qty_purchased'],max(df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
   #  df_all['qty_real_demand'] = np.where(df_all['qty_system_demand']!=0,np.where(df_all['qty_system_demand']>df_all['qty_purchased'],df_all['qty_system_demand'],df_all['qty_purchased']),
   #                                       np.where(df_all['supply_qyt_sys_demand']>df_all['supply_qyt_purchased'],df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    #发生采购了，就去采购数。未采购未产生补单的情况，取系统需求数，未采购产生补单的情况下取补单的大值
    df_all['qty_real_demand']=np.where(df_all['qty_purchased']!=0,df_all['qty_purchased'],np.where(df_all['qty_system_demand']!=0,df_all['qty_system_demand'],
                            np.where(df_all['supply_qyt_sys_demand'] >df_all['supply_qyt_purchased'],df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased'])))
    #特殊情况系统需求0，采购0，未产生补单，取人工需求
    df_all['qty_real_demand']= np.where((pd.isnull(df_all['qty_real_demand']))|(df_all['qty_real_demand']==0),df_all['qty_purchase_demand'],df_all['qty_real_demand'])
    df_temp=df_all.pop('qty_real_demand')
    df_all.insert(8,'qty_real_demand',df_temp)
    df_all=df_all.drop_duplicates('uuid')  #逼不得已采取去重的垃圾方法
    df_storage=get_storage_time(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn)
    df=df_all.merge(df_storage,on='uuid',how='inner')
    df.loc[df['status']==3,'apply_quantity']=0


    df['create_date'] = pd.to_datetime(df['create_date'])
    df['create_time'] = pd.to_datetime(df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['real_logi_time'] = pd.to_datetime(df['real_logi_time'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    df[['create_time', 'purchase_time', 'real_logi_time', 'signed_time', 'storage_time']] =df[['create_time', 'purchase_time', 'real_logi_time', 'signed_time', 'storage_time']].values.astype('<M8[D]')



    df['<1day_purchased'] = np.where(df['purchase_time'] - df['create_time'] < timedelta(days=1), df['qty_purchased'], 0)
    df['<2day_purchased'] = np.where(df['purchase_time'] - df['create_time'] < timedelta(days=2), df['qty_purchased'], 0)
    df['<1day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=1), df['qty_purchased'], 0)
    df['<2day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=2), df['qty_purchased'], 0)
    df['<3day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=3), df['qty_purchased'], 0)
    df['<4day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=4), df['qty_purchased'], 0)
    df['<5day_log'] = np.where(df['real_logi_time'] - df['create_time'] < timedelta(days=5), df['qty_purchased'], 0)
    df['<2day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=2), df['qty_purchased'], 0)
    df['<3day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=3), df['qty_purchased'], 0)
    df['<4day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=4), df['qty_purchased'], 0)
    df['<5day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=5), df['qty_purchased'], 0)
    df['<6day_signed'] = np.where(df['signed_time'] - df['create_time'] < timedelta(days=6), df['qty_purchased'], 0)
    df['<2day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=2), df['qty_received'], 0)
    df['<3day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=3), df['qty_received'], 0)
    df['<4day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=4), df['qty_received'], 0)
    df['<5day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=5), df['qty_received'], 0)
    df['<6day_storage'] = np.where(df['storage_time'] - df['create_time'] < timedelta(days=6), df['qty_received'], 0)
    #KA系统单
    df_10 = df[(df['is_cooperative_supplier'] == 1) & (df['source_type'] == 0)]
    df_10=get_po_kpi(start,df_10,1)
    df_10['flag']=u'KA系统单'
    #非KA系统单
    df_00 = df[(df['is_cooperative_supplier'] == 0) & (df['source_type'] == 0)]
    df_00 = get_po_kpi(start, df_00, 1)
    df_00['flag'] = u'非KA系统单'
    #非KA补单
    df_02 = df[(df['is_cooperative_supplier'] == 0) & (df['source_type'] == 2)]
    df_02= get_po_kpi(start, df_02, 1)
    df_02['flag'] = u'非KA补单'

    df_KPI=pd.concat([df_10,df_00,df_02])
    return df,df_KPI

#所有日期已转换为yyyy-mm-dd格式,下面的指标：t0,指的是当天，t1指的是第二天,interval指的是取几天的平均
def get_po_kpi(start,df,interval):

    df = df[(df['create_time'] >= start)&(df['create_time'] < (start+timedelta(days=interval)))]
    qty_demand = df['qty_real_demand'].sum()


    order_rate_t0=df['<1day_purchased'].sum()/qty_demand
    order_rate_t1=df['<2day_purchased'].sum()/qty_demand
    log_rate_t0=df['<1day_log'].sum()/qty_demand
    log_rate_t1=df['<2day_log'].sum()/qty_demand
    log_rate_t2=df['<3day_log'].sum()/qty_demand
    log_rate_t3=df['<4day_log'].sum()/qty_demand
    log_rate_t4 = df['<5day_log'].sum() / qty_demand
    signed_rate_t1=df['<2day_signed'].sum()/qty_demand
    signed_rate_t2=df['<3day_signed'].sum()/qty_demand
    signed_rate_t3=df['<4day_signed'].sum()/qty_demand
    signed_rate_t4=df['<5day_signed'].sum()/qty_demand
    signed_rate_t5 = df['<6day_signed'].sum()/qty_demand
    storage_rate_t1=df['<2day_storage'].sum()/qty_demand
    storage_rate_t2= df['<3day_storage'].sum()/qty_demand
    storage_rate_t3= df['<4day_storage'].sum()/qty_demand
    storage_rate_t4= df['<5day_storage'].sum()/qty_demand
    storage_rate_t5 = df['<6day_storage'].sum()/qty_demand

    df=pd.DataFrame([[order_rate_t0,order_rate_t1],[log_rate_t0,log_rate_t1,log_rate_t2,log_rate_t3,log_rate_t4],[np.NaN,signed_rate_t1,signed_rate_t2,signed_rate_t3,signed_rate_t4,signed_rate_t5],
                     [np.NaN,storage_rate_t1, storage_rate_t2, storage_rate_t3, storage_rate_t4,storage_rate_t5]],columns=['T1','T2','T3','T4','T5','T6'],index=[u'采购完成率',u'物流发货率',u'签收完成率',u'入库率'])
    return df

def get_storage_time(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    SELECT po.uuid,CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(po.purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(st.create_time,'+00:00','+8:00') as person_logi_time, CONVERT_TZ(st2.create_time,'+00:00','+8:00') as sys_logi_time,CONVERT_TZ(po.signed_time,'+00:00','+8:00') as signed_time,
    CONVERT_TZ(po.complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(storage_time,'+00:00','+8:00') as storage_time,state,is_cooperative_supplier
    from
    purchase_order as po 
    left join purchase_order_shipping_tickets as post on po.id=post.purchaseorder_id
    left join shipping_ticket as st on post.shippingticket_id=st.id
    left join supply_order as so on po.supply_order_id=so.id
    left join shipping_ticket_supply_orders as stso on so.id=stso.supplyorder_id
    left join shipping_ticket as st2 on stso.shippingticket_id=st2.id
    left join
    (select object_id,max(create_time) as storage_time
    from log_logmessage  
    where content like '%入库%'
    group by object_id
    ) as logl on po.id=logl.object_id
    where po.create_time between '{}' and '{}' and po.source_type in (0,2) 
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    #保留时间物流时间较早的
    df.drop_duplicates('uuid',inplace=True)

    df['create_time'] = pd.to_datetime(df['create_time'])
    df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    df['person_logi_time'] = pd.to_datetime(df['person_logi_time'])
    df['sys_logi_time'] = pd.to_datetime(df['sys_logi_time'])
    df['signed_time'] = pd.to_datetime(df['signed_time'])
    df['complete_time'] = pd.to_datetime(df['complete_time'])
    df['storage_time'] = pd.to_datetime(df['storage_time'])
    #物流单号生成时间
    df['real_logi_time']=np.where((pd.notnull(df['person_logi_time']))&(pd.notnull(df['sys_logi_time'])),np.where(df['person_logi_time']<df['sys_logi_time'],df['person_logi_time'],df['sys_logi_time']),
                                  np.where(pd.isnull(df['person_logi_time']),df['sys_logi_time'],df['person_logi_time']))
    #未采购 已采购 全部缺货情况下没有入库时间
    df.loc[(df['state'] == 0)|(df['state'] == 1)|(df['state'] == 6),'storage_time'] = pd.NaT
    #无签收时间取入库时间
    df['signed_time'] = np.where(pd.isnull(df['signed_time']), df['storage_time'], df['signed_time'])

    df['real_logi_time']=np.where(pd.isnull(df['purchase_time']),pd.NaT,df['real_logi_time'])
    # KA的发货时间：有发货的，定义为创建时间采购时间发货
    df['real_logi_time'] = np.where((df['is_cooperative_supplier'] == 1)&(pd.notnull(df['signed_time']))& (pd.isnull(df['real_logi_time'])),df['purchase_time'],df['real_logi_time'])

    #print df['real_logi_time'].dtype
    #非KA无物流时间，取签收时间和购买时间的中值
    df['real_logi_time'] = np.where((df['is_cooperative_supplier'] == 0)&(pd.notnull(df['purchase_time'])) & (pd.notnull(df['signed_time'])) & (pd.isnull(df['real_logi_time'])),df['purchase_time'] + (df['signed_time'] - df['purchase_time']) / 2, df['real_logi_time'])
    #经测试，物流时间有可能早于采购时间，只好取采购时间
    df['real_logi_time'] = np.where(df['real_logi_time'] < df['purchase_time'], df['purchase_time'],df['real_logi_time'])


    #df.to_excel(u'F:\createExcel\采购完成率test2原来原来.xlsx', index=False, encoding='utf8')

    df=df[['uuid','real_logi_time','signed_time','storage_time']]

    return df

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()
    start='2017-11-15'
    end='2017-11-16'
    start = datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.strptime(end, "%Y-%m-%d").date()
    path = r'F:\createExcel\采购完成率一天' + str(start) + '.xlsx'
    path = unicode(path, "utf-8")
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    for i in range(3):
        df,df_KPI=get_po_info(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
        #print df_all.head()

        #df.to_excel(writer, sheet_name=u'rawdata',index=False, encoding='utf8')
        df_KPI.to_excel(writer,sheet_name=str(start)+u'完成率',encoding= 'utf8')

        start=start+timedelta(days=1)
        end=end+timedelta(days=1)


    writer.save()