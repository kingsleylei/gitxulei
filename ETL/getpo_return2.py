#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlsxwriter
def get_po_info(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    SELECT  date_format(CONVERT_TZ(po.create_time,'+00:00','+8:00'),"%Y-%m-%d") as po_create_date, date_format(CONVERT_TZ(gr.create_time,'+00:00','+8:00'),"%Y-%m-%d") as gr_create_date,pro.item_no,gr.application_id,po.uuid,purchase_company_name,is_cooperative_supplier,purchase_price,return_quantity,
    gr.status,qty_system_demand,qty_purchased,category_name,reason_detail
    from purchase_order as po 
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    join goods_return as gr on po.id=gr.purchase_order_id
    join return_reason as rr on gr.id=rr.return_application_id
    where CONVERT_TZ(gr.create_time,'+00:00','+8:00') BETWEEN '{}' and '{}'	
    							

    """.format(start,end)
    df_yh=pd. read_sql(sql, idoo_yh_conn)
    df_yh['warehouse']=u'余杭'

    df_xs=pd. read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn=pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all=pd. concat([df_yh,df_xs, df_hn], ignore_index=True)
    return df_all


if __name__ == '__main__':

    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    start='2017-10-16'#16号0点开始
    end='2017-10-23'#注意结束时间 23号0点结束
    start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
    df_all=get_po_info(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    #df_all.to_excel('F:\createExcel\get_po_info.xlsx',index=False)

    path = r'F:\createExcel\getpo_return'+str(start)+'-'+str(end)+'.xlsx'
    writer = pd.ExcelWriter(path,engine='xlsxwriter', options={'strings_to_urls': False})
    df_all.to_excel(writer, index=False, encoding='utf8')
    writer.save()