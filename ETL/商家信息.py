#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter

pd.options.mode.chained_assignment = None  # default='warn'


def get_po_rawdata(start, end, idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select date_format(CONVERT_TZ(po.create_time,'+00:00','+8:00'),"%Y-%m-%d") as create_date,po.uuid,qty_system_demand,qty_purchase_demand,qty_purchased,qty_received,supply_qyt_sys_demand,supply_qyt_purchased,
    purchase_price,qty_purchased*purchase_price as purchase_amount, CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(signed_time,'+00:00','+8:00') as signed_time,timestampdiff(SECOND,po.create_time,purchase_time)/3600 as order_interval,
    purchase_company_name,is_cooperative_supplier,apply_quantity,gr.status,sku_no,item_no,source_type,po.state,po.prev_state,po.follow_user_name,category_name
    from
    (select * from
    (select *
    from purchase_order
    where CONVERT_TZ(create_time,'+00:00','+8:00') between '{}' and '{}' and source_type in (0,2) and state !=7) as b
    left join 
    (select source_purchase_order_id as sp_order_id,sum(qty_system_demand) as supply_qyt_sys_demand,sum(qty_purchased) as supply_qyt_purchased
    from purchase_order group by source_purchase_order_id
    ) as c on b.id=c.sp_order_id) as po 
    left join 	
    goods_return as gr on po.id=gr.purchase_order_id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id

    """.format(start, end)
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)

    # df_all['qty_real_demand']=np.where(df_all['qty_system_demand']!=0,df_all['qty_purchased'],max(df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_all['qty_real_demand'] = np.where(df_all['qty_system_demand'] != 0,
                                         np.where(df_all['qty_system_demand'] > df_all['qty_purchased'],
                                                  df_all['qty_system_demand'], df_all['qty_purchased']),
                                         np.where(df_all['supply_qyt_sys_demand'] > df_all['supply_qyt_purchased'],
                                                  df_all['supply_qyt_sys_demand'], df_all['supply_qyt_purchased']))
    df_temp = df_all.pop('qty_real_demand')
    df_all.insert(8, 'qty_real_demand', df_temp)
    # 取得完整的采购时间轴
   # df_storage = get_storage_time(start, end, idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    #df = df_all.merge(df_storage, on='uuid', how='inner')
    df = df_all.drop_duplicates('uuid', keep='last')

   # del df['signed_time_ch']
    df.loc[df['status'] == 3, 'apply_quantity'] = 0
    # df = df[(df['is_cooperative_supplier'] == 1) & (df['source_type'] == 0)]
    # df['create_date'] = pd.to_datetime(df['create_date'])
    # df['create_time'] = pd.to_datetime(df['create_time'])
    # df['purchase_time'] = pd.to_datetime(df['purchase_time'])
    # df['real_logi_time'] = pd.to_datetime(df['real_logi_time'])
    # df['signed_time'] = pd.to_datetime(df['signed_time'])
    # df['storage_time'] = pd.to_datetime(df['storage_time'])
    # df['complete_time'] = pd.to_datetime(df['complete_time'])
    # df['logistics_interval'] = [(td.seconds + td.days * 24 * 3600) / 3600 for td in
    #                             (df['real_logi_time'] - df['create_time'])]
    df_sup=get_company_info()
    del df_sup['category_name']
    df=df.merge(df_sup,left_on='purchase_company_name',right_on='company_name',how='left')
    #print df.head()
    df=df[['create_time','uuid','item_no','qty_purchased','purchase_amount','purchase_company_name','home_url','category_name']]
    return df

def get_company_info():
    supp_conn=db_help.get_idoo_supplier()
    sql="""
    select company_name,home_url,biz_place,detail_address,CASE company_source 
	WHEN 1 THEN '1688'
	when 2 then '淘宝'
	when 3 then '天猫'
	else  '其他' end company_source,phone
    from supplier as sup 
    left join supplier_account as sa on sup.company_name=sa.supplier_name
    """
    df=pd.read_sql(sql,supp_conn)
    df['company_source']=np.where(df['home_url'].str.find('1688')!=-1,'1688',df['company_source'])
    df['temp']=df['biz_place'].str.replace(u'中国','')
    df['temp']=df['temp'].str.replace('/',' ')
    df['temp']=df['temp'].str.strip()
    df['temp']=df['temp'].str.split(' ') #得到列表
    #列表中有Null需要判断
    df['province']=map(lambda s:s[0] if isinstance(s,list) else np.NaN,df['temp'])
    df['city'] = map(lambda s: s[1] if isinstance(s, list) and len(s)>=2 else np.NaN, df['temp'])
    df = df.drop_duplicates('company_name')
    df_cate=get_company_cate(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    df=df.merge(df_cate,on='company_name',how='left')
    df=df[['company_name','home_url','biz_place','province','city','detail_address','company_source','phone','category_name']]
    return df
 #贴上时间加快查询速度，
def get_company_cate(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select  distinct purchase_company_name,category_name 
    from purchase_order as po
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    
    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df=df.drop_duplicates('purchase_company_name', keep='last')
    df.rename(columns={'purchase_company_name':'company_name'},inplace=True)

    return df
if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()

    start=datetime.now().date()-timedelta(days=1)
    end=datetime.now().date()


    df_raw=get_po_rawdata(start,end,idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    df_sup=get_company_info()
    # #print df_all.head()

    writer = pd.ExcelWriter(u'F:\createExcel\商家信息3.xlsx', engine='xlsxwriter', options={'strings_to_urls': False})
    df_sup.to_excel(writer, sheet_name=u'商家信息', index=False, encoding='utf8')
    writer.save()

    writer2 = pd.ExcelWriter(u'F:\createExcel\采购信息3'+str(start)+'.xlsx', engine='xlsxwriter', options={'strings_to_urls': False})
    df_raw.to_excel(writer2, sheet_name=u'采购信息', index=False, encoding='utf8')
    writer2.save()