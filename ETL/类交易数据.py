#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlrd
import xlwt


#item曝光数——PV
def get_ga_PV_info(web_conn,start,end):
    sql = '''
        select 
            ua.pno,count(*) as PV
        FROM 
            user_action as ua
        where 
            ua.time_stamp >= '{}' 
            and ua.time_stamp < '{}' 
            and ua.event = 'Impression'
        GROUP BY ua.pno
        '''.format(start,end)
    df = pd.read_sql(sql, con=web_conn)    
    return df


#item点击数
def get_ga_clickOFitem_info(web_conn,start,end):
    sql = '''
        select 
            ua.pno,count(*) as click
        FROM 
            user_action as ua
        where 
            ua.time_stamp >= '{}' 
            and ua.time_stamp < '{}' 
            and ua.event = 'click'
        GROUP BY ua.pno
        '''.format(start,end)
    df = pd.read_sql(sql, con=web_conn)    
    return df


#item成交数，用于算购买转化率
def get_ga_dealOFUV_info(web_conn,start,end):
    sql = '''
        select 
            ua.pno,sum(item_num) as deal_num
        FROM 
            user_action as ua
        where 
            ua.time_stamp >= '{}' 
            and ua.time_stamp < '{}' 
            and ua.event = 'purchase'
        GROUP BY ua.pno
        '''.format(start,end)
    df = pd.read_sql(sql, con=web_conn)    
    return df


#被购买过的item
def get_ga_purchaseOFitem_info(web_conn,start,end):
    sql = '''
        select 
            DISTINCT ua.pno
        FROM 
            user_action as ua
        where 
            ua.time_stamp >= '{}' 
            and ua.time_stamp < '{}' 
            and ua.event = 'purchase'
        GROUP BY ua.pno
        '''.format(start,end)
    df = pd.read_sql(sql, con=web_conn)    
    return df


def get_so_info(idoo_conn, idoo_xs_conn,idoo_hn_conn,start,end):
    sql = '''
    select 
            sol.item_no,so.order_name,sol.product_qty,so.login,
            sol.price_real*sol.product_qty as amount
    FROM 
            sale_order as so 
	         join sale_order_line as sol on so.order_name = sol.order_name
            join payment_transaction as py on so.order_name = py.order_name
	 where 
            so.state in ('on hold','pay success','delay') 
            and py.update_at >= '{}' 
            and py.update_at < '{}' 
            and sol.is_delivery = 0
            and sol.product_qty > 0
        '''.format(start,end)
    df_yh = pd.read_sql(sql, con=idoo_conn)    
    df_xs = pd.read_sql(sql, con=idoo_xs_conn)
    df_hn = pd.read_sql(sql, con=idoo_hn_conn)
    df_temp = df_yh.append(df_xs,ignore_index = True)
    df_all = df_temp.append(df_hn,ignore_index = True)
    return df_all


def get_saleing_product_no(app_conn):
    sql_product ='''
    select 
			 pt.product_no as pno
    from 
          product_template as pt
    where 
          pt.sale_ok = 't'
    '''
    df_product_no= pd.read_sql(sql_product,con=app_conn)
    return df_product_no





def merge_all_df(PV_df_web,deal_df_web,clickOFitem_df,purchaseOFitem_df):
    empty_data = PV_df_web['pno'] == ''
    PV_df_web_new = PV_df_web[~empty_data]
    empty_data = deal_df_web['pno'] == ''
    deal_df_web_new = deal_df_web[~empty_data]
    empty_data = clickOFitem_df['pno'] == ''
    clickOFitem_df_new = clickOFitem_df[~empty_data]
    empty_data = purchaseOFitem_df['pno'] == ''
    purchaseOFitem_df_new = purchaseOFitem_df[~empty_data]
    
    PV_click_df = pd.merge(PV_df_web_new,clickOFitem_df_new,how='left',on='pno')
    PV_click_deal_df = pd.merge(PV_click_df,deal_df_web_new,how='left',on='pno')
    PV_click_deal_purchase_df = pd.merge(PV_click_deal_df,purchaseOFitem_df_new,how='left',on='pno')
    
    return PV_click_deal_purchase_df



def get_category_one(app_conn):
   sql_category_one='''
    SELECT 
            distinct id as id_one, name as Category_one 
    from 
            product_public_category as ppc 
    WHERE 
            parent_id is  null
   '''
   cate_one = pd.read_sql(sql_category_one,con=app_conn)
   
   parent_id = cate_one['id_one']
   sql ='''
    SELECT 
            distinct id as id_two,name as Category_two ,parent_id as id_one 
    from 
            product_public_category as ppc 
    WHERE 
            parent_id in ({})
    '''.format(','.join("'"+str(b)+"'" for b in parent_id))
   cate_two = pd.read_sql(sql,con=app_conn)

   parent_id_second = cate_two['id_two']
   sql_three ='''
    SELECT 
            distinct id as id_three,name as Category_three ,parent_id as id_two 
    from 
            product_public_category as ppc 
    WHERE 
            parent_id in ({})
     '''.format(','.join("'"+str(b)+"'" for b in parent_id_second))
   cate_three = pd.read_sql(sql_three,con=app_conn)
   
   return cate_one,cate_two,cate_three

def get_product_no(app_conn,item_no_list):
    sql_product ='''
    select 
            ppcptr.product_public_category_id as ppc_id, pt.product_no 
    from 
            product_template as pt
    join product_public_category_product_template_rel as ppcptr on pt.id=ppcptr.product_template_id
    where  
            pt.product_no in ({})
    '''.format(','.join("'"+ str(item_no) +"'" for item_no in item_no_list ))
    df_product_no= pd.read_sql(sql_product,con=app_conn)
    return df_product_no


def category_product_on(app_conn,cate_one,cate_two,cate_three,item_no_list):
     #product_one_id = cate_one['id_one']
     #one_product =get_product_no(odoo_conn,product_one_id)
     #one_product.columns =['id_one','product_no']
     ppcid_product =get_product_no(app_conn,item_no_list)
     ppcid_product.columns =['id_X','product_no']
    
     two_name_product =pd.merge(ppcid_product,cate_two,left_on='id_X',right_on='id_two',how='left')
     one_two_name_product = pd.merge(two_name_product,cate_one,how = 'left',on = 'id_one')
     del one_two_name_product['id_one'],one_two_name_product['id_two']
     #tttt = one_two_name_product.drop_duplicates(['product_no']) 
     NONE = one_two_name_product['category_two'].isnull()
     one_two_name_product_none = one_two_name_product[NONE]
     one_two_name_product_unnone = one_two_name_product[~NONE]
     
     one_two_name_product_supply = category_product_supply(app_conn,cate_one,cate_two,cate_three,one_two_name_product_none)
     one_two_name_product_unnone_all = one_two_name_product_unnone.append(one_two_name_product_supply,ignore_index = True)
     return one_two_name_product_unnone_all
  
      
def category_product_supply(app_conn,cate_one,cate_two,cate_three,one_two_name_product_none):
     #product_one_id = cate_one['id_one']
     #one_product =get_product_no(odoo_conn,product_one_id)
     #one_product.columns =['id_one','product_no']
     #ppcid_product =get_product_no(app_conn,item_no_list)
     #ppcid_product.columns =['id_X','product_no']
     del one_two_name_product_none['category_one'],one_two_name_product_none['category_two']
     three_name_product =pd.merge(one_two_name_product_none,cate_three,left_on='id_X',right_on='id_three',how='left')
     two_three_name_product = pd.merge(three_name_product,cate_two,how = 'left',on = 'id_two')
     one_two_three_name_product = pd.merge(two_three_name_product,cate_one,how = 'left',on = 'id_one')
     del one_two_three_name_product['id_one'],one_two_three_name_product['id_two'],one_two_three_name_product['id_three']
     category_mens_list = one_two_three_name_product['category_two'].isin(['Men\'s Clothing','Men\'s Shoes','mens_bags'])
     temp_category_mens = one_two_three_name_product[category_mens_list]
     del temp_category_mens['category_one']
     temp_category_mens.rename(columns = {'category_two':'category_one','category_three':'category_two'},inplace=True)
     
     temp_category_not_mens = one_two_three_name_product[~category_mens_list]
     del temp_category_not_mens['category_three']
     one_two_three_name_product_new = temp_category_mens.append(temp_category_not_mens,ignore_index = True)
     return one_two_three_name_product_new

def stas_so(df_so):
    df_so_group = df_so.groupby(['item_no'])
    #customer_num = df_so_group['login'].count()
    order_num = df_so_group['order_name'].count()
    index_item = order_num.index
    quantity_item = df_so_group['product_qty'].sum()
    amount = df_so_group['amount'].sum()
    
    df_frame = {u'item_no':index_item,u'单数':order_num,u'件数'
                :quantity_item,u'总金额':amount}
    df_final = pd.DataFrame(df_frame,columns = [u'item_no',u'单数',u'件数',u'总金额'])
    
    return df_final



def category_main(df_all,app_conn):
    
    df_drop = df_all.drop_duplicates(['pno'])
    NONE = df_drop['pno'].isnull()
    df_drop = df_drop[~NONE]
    
    item_no_list = df_drop['pno']
    cate_one,cate_two,cate_three = get_category_one(app_conn)
    product_category_have = category_product_on(app_conn,cate_one,cate_two,cate_three,item_no_list)
        
    product_category = product_category_have.drop_duplicates(['product_no'])      #存在脏数据，同一个货号对应至少两个二级品类
    del product_category['id_X']
    
    return product_category


def people_category(df_so,product_category,grade):
    df_merge = pd.merge(product_category,df_so,how='left',left_on='product_no',right_on='item_no')
    if grade ==2:
        #两级分组
        df_group_two = df_merge.groupby(['category_one','category_two'],as_index = False)
        
        #获取分组的索引
        temp_df = df_group_two['product_no'].count()
        index_category_one = temp_df['category_one']
        index_category_two= temp_df['category_two']
        
        df_group = df_merge.groupby(['category_one','category_two'])
        customer_num = []
        for name,group in df_group:
            temp = group.drop_duplicates(['login'])['login'].count()
            customer_num.extend([temp])
            
        df_frame = {u'一级类目':index_category_one,u'二级类目':index_category_two,
                   u'人数':customer_num}
        df_final = pd.DataFrame(df_frame,columns = [u'一级类目',u'二级类目',u'人数'])
        return df_final
    else:
        #一级分组
        df_group_one = df_merge.groupby(['category_one'],as_index = False)
        
        #获取分组的索引
        temp_df = df_group_one['product_no'].count()
        index_category_one = temp_df['category_one']
        
        df_group = df_merge.groupby(['category_one'])
        customer_num = []
        for name,group in df_group:
            temp = group.drop_duplicates(['login'])['login'].count()
            customer_num.extend([temp])
            
        df_frame = {u'一级类目':index_category_one,u'人数':customer_num}
        df_final = pd.DataFrame(df_frame,columns = [u'一级类目',u'人数'])
        return df_final

def huizong(df,df_people_num,level):
    #df = product_category_expo_saling_so
    
    df['amount_price'] = df['purchase_price']*df['deal_num']
    if level == 2:
        df = pd.merge(df,df_people_num,how='left',left_on = ['category_one','category_two'],right_on = [u'一级类目',u'二级类目'])
        df_group = df.groupby(['category_one','category_two'],as_index = False)
    
        #获取分组的索引
        temp_df = df_group['product_no'].count()
        index_category_one = temp_df['category_one']
        index_category_two= temp_df['category_two']
    
        df_group = df.groupby(['category_one','category_two'])
        #客成交额，成交件数，顾客数
        amount = df_group[u'总金额'].sum().reset_index(drop=True)
        quntity = df_group[u'件数'].sum()
        customer_num = df_group[u'人数'].sum().reset_index(drop=True)
        cost = df_group[u'amount_price'].sum()
        
        #客单价
        pct = []
        for i in range(len(amount)):
            if customer_num[i] == 0:
                temp = 0
            else:
                temp = float(amount[i])/float(customer_num[i])
            pct.extend([temp])
        #pct = map(lambda x,y:float(x)/float(y),amount,customer_num)
    
        #PV
        PV_num = df_group[u'pv'].sum()
    
        #点击货的数量,成交货号数,加购数量,被购买过的item
        #PVOFitem_num = df_group[u'pv'].count()
        click_num = df_group[u'click'].sum()
        deal_num = df_group[u'deal_num'].sum()
        purchase_item_num = df_group[u'tag_PUR'].sum()

        #print PV_num.values
        #购买转化率
        pcr = map(lambda x,y:float(x)/float(y),deal_num,click_num)

        #动销率
        #dxr = map(lambda x,y:float(x)/float(y),purchase_item_num,click_num)
    
        data_frame = {u'一级类目':index_category_one,u'二级类目':index_category_two,
                      u'曝光':PV_num.values,u'购买转化率':pcr,u'客单价':pct,
                      u'成交额':amount.values,u'采购成本':cost.values,u'成交件数':quntity.values,
                      u'详情页点击量':click_num.values,u'被售货号':purchase_item_num.values}
        df_finall = pd.DataFrame(data_frame,columns = [u'一级类目',u'二级类目',u'曝光',
                        u'购买转化率',u'客单价',u'成交额',u'采购成本',u'成交件数',u'详情页点击量',u'被售货号'])
    
        return df_finall
    else:
        df_group = df.groupby(['category_one'],as_index = False)
    
        #获取分组的索引
        temp_df = df_group['product_no'].count()
        index_category_one = temp_df['category_one']
    
        df_group = df.groupby(['category_one'])
        #客成交额，成交件数，顾客数
        amount = df_group[u'总金额'].sum()
        quntity = df_group[u'件数'].sum()
        #customer_num = df_group[u'人数'].sum()
        cost = df_group[u'amount_price'].sum()
        
        #客单价
        #pct = map(lambda x,y:float(x)/float(y),amount,customer_num)
    
        #PV
        PV_num = df_group[u'pv'].sum()
    
        #点击货的数量,成交货号数,加购数量
        #PVOFitem_num = df_group[u'pv'].count()
        click_num = df_group[u'click'].sum()
        deal_num = df_group[u'deal_num'].sum()
        purchase_item_num = df_group[u'tag_PUR'].sum()
        
        #print PV_num.values
        #购买转化率
        pcr = map(lambda x,y:float(x)/float(y),deal_num,click_num)
    
        data_frame = {u'一级类目':index_category_one,
                      u'曝光':PV_num.values,u'购买转化率':pcr,
                      u'成交额':amount.values,u'采购成本':cost.values,u'成交件数':quntity.values,
                      u'详情页点击量':click_num.values,u'被售货号':purchase_item_num.values}
        df_finall = pd.DataFrame(data_frame,columns = [u'一级类目',u'曝光',
                        u'购买转化率',u'成交额',u'采购成本',u'成交件数',u'详情页点击量',u'被售货号'])
        df_finall_new = pd.merge(df_finall,df_people_num,how='left',on=u'一级类目')
       
        #客单价
        pct = []
        for i in range(len(df_finall_new[u'成交额'])):
            if df_finall_new[u'人数'][i] == 0:
                temp = 0
            else:
                temp = float(df_finall_new[u'成交额'][i])/float(df_finall_new[u'人数'][i])
            pct.extend([temp])
        df_finall_new[u'客单价'] = pct
        del df_finall_new[u'人数']
        
        return df_finall_new


def cal_shangjia(df,grade):
    #df = product_category_saling
    if grade == 2:
        df_group = df.groupby(['category_one','category_two'],as_index = False)
        
        #获取分组的索引
        temp_df = df_group['product_no'].count()
        index_category_one = temp_df['category_one']
        index_category_two= temp_df['category_two']
        
        #上架商品数
        df_group = df.groupby(['category_one','category_two'])
        pno_num = df_group[u'product_no'].count()
    
        data_frame = {u'一级类目':index_category_one,u'二级类目':index_category_two,
                      u'在售商品数':pno_num.values}
        df_finall = pd.DataFrame(data_frame,columns = [u'一级类目',u'二级类目',u'在售商品数'])
        
        return df_finall
    else:
        df_group = df.groupby(['category_one'],as_index = False)
        
        #获取分组的索引
        temp_df = df_group['product_no'].count()
        index_category_one = temp_df['category_one']
        
        #上架商品数
        df_group = df.groupby(['category_one'])
        pno_num = df_group[u'product_no'].count()
    
        data_frame = {u'一级类目':index_category_one,
                      u'在售商品数':pno_num.values}
        df_finall = pd.DataFrame(data_frame,columns = [u'一级类目',u'在售商品数'])
        
        return df_finall

def cal_dxr_avgpv(df_zong):
    #动销率
    df_zong[u'动销率'] = map(lambda x,y:float(x)/float(y),df_zong[u'被售货号'],df_zong[u'在售商品数'])

    #动销率
    df_zong[u'平均曝光率'] = map(lambda x,y:float(x)/float(y),df_zong[u'曝光'],df_zong[u'在售商品数'])
    
    return df_zong


def get_sku_id_info(idoo_conn, idoo_xs_conn,idoo_hn_conn,item_no_list):
    
    sql = '''
        select 
            sku.sku_id,pro.item_no 
        from 
            stock_keep_unit as sku
            join product as pro on pro.id = sku.product_id
        where 
            pro.item_no in ({})
        '''.format(','.join("'"+ str(item_no) +"'" for item_no in item_no_list ))
    df_yh = pd.read_sql(sql, con=idoo_conn)    
    df_xs = pd.read_sql(sql, con=idoo_xs_conn)
    df_hn = pd.read_sql(sql, con=idoo_hn_conn)
    df_temp = df_yh.append(df_xs,ignore_index = True)
    df_all = df_temp.append(df_hn,ignore_index = True)
    df_all = df_all.drop_duplicates(['sku_id','item_no'])
    return df_all

def get_sku_price_info(supplier_conn,id_list):
    
    sql = '''
        select 
            slr.price as purchase_price,slr.stock_keep_unit_id as sku_id
        from 
            sku_link_rel as slr
        where 
            slr.stock_keep_unit_id in ({})
            and slr.is_default = 1
        '''.format(','.join("'"+ str(sku_id) +"'" for sku_id in id_list ))
    df_yh = pd.read_sql(sql, con=supplier_conn)    
    df_xs = pd.read_sql(sql, con=supplier_conn)
    df_hn = pd.read_sql(sql, con=supplier_conn)
    df_temp = df_yh.append(df_xs,ignore_index = True)
    df_all = df_temp.append(df_hn,ignore_index = True)
    df_all = df_all.drop_duplicates(['purchase_price','sku_id'])
    return df_all

if __name__ == '__main__':
    odoo_conn = db_help.get_odoo_conn()
    web_conn = db_help.get_web_db_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_conn  =  db_help.get_new_idoo_conn()
    supplier_conn  = db_help.get_new_supplier_idoo_conn()
    idoo_hn_conn = db_help.get_new_idoo_hn_conn()
    app_conn = db_help.get_app_conn()
    
    start_date = '2017-11-14 00:00:00'                                               #使用查询当天的时间
    end_date   = '2017-11-13 00:00:00'                                               #使用查询当天+1的时间
    start = datetime.datetime.strptime(start_date,"%Y-%m-%d %H:%M:%S")        #采购单生成计算时间
    end = datetime.datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")  
    
    
    #获取商品的曝光数据:浏览。
    PV_df_web = get_ga_PV_info(web_conn,start,end)
   
    #item购买数量
    deal_df_web = get_ga_dealOFUV_info(web_conn,start,end)
    empty_data = deal_df_web['pno'] == ''
    deal_df_web = deal_df_web[~empty_data]
    item_no_list_sku = deal_df_web['pno']
    df_id_item = get_sku_id_info(idoo_conn, idoo_xs_conn,idoo_hn_conn,item_no_list_sku)
    #df_id_item = df_id_item.drop_duplicates(['sku_id','item_no'])
    id_list = df_id_item['sku_id']
    df_id_price = get_sku_price_info(supplier_conn,id_list)
    df_item_price = pd.merge(df_id_item,df_id_price,how='left',on= 'sku_id')
    del df_item_price['sku_id']
    
    #去重求平均，因为一个item对应多个sku
    df_item_price_group = df_item_price.groupby(['item_no'])
    avg_purch_price = df_item_price_group['purchase_price'].mean()
    index = avg_purch_price.index
    data_frame = {u'item_no':index,u'purchase_price':avg_purch_price}
    df_item_price_new = pd.DataFrame(data_frame,columns = [u'item_no',u'purchase_price'])
    
    deal_df_web_price= pd.merge(deal_df_web,df_item_price_new,how='left',left_on= 'pno',right_on='item_no')
    del deal_df_web_price['item_no']
    
    #item点击数
    clickOFitem_df = get_ga_clickOFitem_info(web_conn,start,end)
    clickOFitem_df['tag_CLICK'] = 1
    
    #被购买过的item
    purchaseOFitem_df = get_ga_purchaseOFitem_info(web_conn,start,end)
    purchaseOFitem_df['tag_PUR'] = 1
    
    #合并所有查询结果
    df_all = merge_all_df(PV_df_web,deal_df_web_price,clickOFitem_df,purchaseOFitem_df)

    #获取曝光商品的一二级品类
    product_category = category_main(df_all,app_conn)
    

    #将品类与曝光数据连接
    product_category_expo = pd.merge(product_category,df_all,how='left',left_on='product_no',right_on='pno')
    del product_category_expo['pno']
    kongdata = product_category_expo['product_no'] == ''
    product_category_expo = product_category_expo[~kongdata]

      
    #从idoo抽取订单号，商品号，购买数量，价格等
    #item_no_list_new = product_category_expo['product_no']
    df_so = get_so_info(idoo_conn,idoo_xs_conn,idoo_hn_conn,start,end)
    df_so_stas = stas_so(df_so)
    
    #将曝光数据和订单数据合并
    product_category_expo_saling_so = pd.merge(product_category_expo,df_so_stas,how='left',left_on='product_no',right_on='item_no')
    del product_category_expo_saling_so['item_no']
    
    #按二级品类进行汇总,特别注意人数去重问题
    level = 2
    df_people_num = people_category(df_so,product_category,level)
    df_huizong_first = huizong(product_category_expo_saling_so,df_people_num,level)
    
    #从odoo抽取在售商品,并与所有商品的品类数据连接
    df_sale_pno = get_saleing_product_no(app_conn)
    df_sale_pno['tag_sale'] = 1
    product_all_category = category_main(df_sale_pno,app_conn)
    product_category_saling = pd.merge(df_sale_pno,product_all_category,how='left',left_on = 'pno',right_on='product_no')
    del product_category_saling['pno']
    df_saling = cal_shangjia(product_category_saling,level)
    
    df_zong = pd.merge(df_huizong_first,df_saling,how='left',on = [u'一级类目',u'二级类目'])
    df_zong = cal_dxr_avgpv(df_zong)
    
    #按一级品类进行汇总,特别注意人数去重问题
    level_one = 1
    df_people_num_one = people_category(df_so,product_category,level_one)
    df_huizong_first_one = huizong(product_category_expo_saling_so,df_people_num_one,level_one)
    
    df_saling_one = cal_shangjia(product_category_saling,level_one)
    df_zong_one = pd.merge(df_huizong_first_one,df_saling_one,how='left',on = [u'一级类目'])
    df_zong_one = cal_dxr_avgpv(df_zong_one)
    
    writer = pd.ExcelWriter(u'E:\\'+start_date.split(' ')[0]+u'类目交易数据.xlsx')
    df_zong.to_excel(writer,u'一二级类目',index=False,encoding= 'utf8')
    df_zong_one.to_excel(writer,u'一级类目',index=False,encoding= 'utf8')
    #df_huizong.to_excel(writer,u'大货',index=False,encoding= 'utf8')
    #df_so.to_excel(writer,u'so',index=False,encoding= 'utf8')
    writer.save()
    
    
    