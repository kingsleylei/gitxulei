#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlsxwriter
def get_KA(start,end,idoo_yh_conn ,idoo_xs_conn ,idoo_hn_conn):
    sql="""
    select distinct purchase_company_name
    from purchase_order
    where is_cooperative_supplier=1
    """
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_hn = pd.read_sql(sql, idoo_hn_conn)

    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all=df_all.drop_duplicates('purchase_company_name')


    sql_cut="""
    select WEEK(CONVERT_TZ(create_time,'+00:00','+8:00'),3)+1 as week_ch,purchase_company_name
    from purchase_order
    where CONVERT_TZ(create_time,'+00:00','+8:00') between '2017-10-02' and '2017-10-30' and is_cooperative_supplier=1
    GROUP BY week_ch,purchase_company_name
    
    """
    df_yh_cut = pd.read_sql(sql_cut, idoo_yh_conn)
    df_xs_cut = pd.read_sql(sql_cut, idoo_xs_conn)
    df_hn_cut = pd.read_sql(sql_cut, idoo_hn_conn)
    df_cut = pd.concat([df_yh_cut, df_xs_cut, df_hn_cut], ignore_index=True)
    df_cut=df_cut.drop_duplicates()
    print df_cut[df_cut.columns]

    return  df_all,df_cut
if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()
    start='2017-10-02'
    end='2017-10-30'
    start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
    df_all,df_cut=get_KA(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)



    # path = r'F:\createExcel\co_supplier'+str(start)+'-'+str(end)+'.xlsx'
    # writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    # df_all.to_excel(writer,'all_co_supplier', index=False, encoding='utf8')
    # df_cut.to_excel(writer, 'part_co_supplier', index=False, encoding='utf8')
    # writer.save()


    # path = r'F:\createExcel\get_po_KPI'+str(start)+'-'+str(end)+'.xlsx'
    # writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    # df_all.to_excel(writer, index=False, encoding='utf8')
    # writer.save()