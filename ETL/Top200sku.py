#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlrd
import xlwt

def topSku(n,start,end,country,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn):
    sql="""
    SELECT sol.create_at,sol.order_name,sku_no,sku_title,sol.item_no as product_no,sol.product_name,sol.image_url,sol.origin_real as product_price, (product_qty*price_real) as price_total
    FROM sale_order_line as sol 
    join sale_order as so on sol.order_name=so.order_name 
    join stock_keep_unit as sku on sol.sku_id=sku.sku_id
    where so.shipping_country='{}' and DATE(CONVERT_TZ(sol.create_at,'+00:00','+8:00'))>'{}' and DATE(CONVERT_TZ(sol.create_at,'+00:00','+8:00'))<'{}'
    and so.state='pay success' and item_no is not null
        
    """.format(country,start,end)
    df_yh=pd.read_sql(sql,idoo_yh_conn)
    #print df_yh.head()
    df_xs=pd.read_sql(sql,idoo_xs_conn)
    df_hn=pd.read_sql(sql,idoo_hn_conn)
    df_all=pd.concat([df_yh,df_xs,df_hn],ignore_index=True)
    #匹配表获取分组统计后额外的字段
    df_match=df_all.drop_duplicates('sku_no',keep='last')[['sku_no','sku_title','product_no','product_name','image_url']]
    df=df_all.groupby('sku_no',as_index=False)['price_total'].sum()
    df=df.merge(df_match,on='sku_no',how='inner')
    df=df.sort_values(by='price_total',ascending=False)[:n]
    return df

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    start='2017-10-01'
    end='2017-10-31'
    start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
    print start
    end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
    df=topSku(200,start,end,'india',idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    #df.to_excel('F:\createExcel\TopSku3.xlsx',index=False)