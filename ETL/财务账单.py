#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import db_help
import xlsxwriter


def get_prpo_info(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
   	SELECT pr.uuid as pr_uuid,pr.supplier_uuid,pr.supplier_name,CONVERT_TZ(pr.create_time,'+00:00','+8:00') as pr_create_time,CONVERT_TZ(pr.pay_time,'+00:00','+8:00') as pay_time ,pr.purchase_rmb,
   	pr.freight_rmb,pr.total_rmb,pr.pay_rmb,bank_name,deposit_bank,bank_account,account_holder,pr.status,po.uuid, date(CONVERT_TZ(po.create_time,'+00:00','+8:00')) as create_time,po.purchase_company_name,'USD' as currency,
   	purchase_price*po.qty_received as purchased_amount,title,pro.item_no,po.qty_received,purchase_price as purchase_price
	from payment_request as pr 	
	join pr_bill as pb on pr.id=pb.pr_id
	join bill on pb.bill_id=bill.id
	join bill_purchase_order as bpo on bill.id=bpo.bill_id
	join purchase_order as po on bpo.po_id=po.id
	join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
	join product as pro on sku.product_id=pro.id
	where pr.status='PR_FINANCE_PASS'
    """

    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    if df.empty:
        exit()
    df_poTrackingNo=getPoTrackingNo(list(df['uuid']))
    df=df.merge(df_poTrackingNo,on='uuid',how='left')

    # 该时间的范围为了寻找so单,加快查询速度
    #df['create_time']=pd.to_datetime(df['create_time'])
    start=df['create_time'].min()-timedelta(days=2)
    end=df['create_time'].max()
    df_soTrackingNo=getSoTrackingNo(start,end)

    #so单生成时间+1天匹配po单生成时间
    df_soTrackingNo['create_time']=pd.to_datetime(df_soTrackingNo['create_time'])
    df_soTrackingNo['create_time']=[t.date()+timedelta(days=1) for t in df_soTrackingNo['create_time']]
    df=df.merge(df_soTrackingNo,on=['item_no','create_time'],how='left')

    #去重
    df = df[df['qty_received'] != 0]
    df.drop_duplicates('uuid', inplace=True)
    df[u'贸易类型']=u'货物贸易'
    #分摊运费等金额以保持与请款单一致
    df_pivot=pd.pivot_table(df,values=['qty_received','purchased_amount'],index='supplier_name',aggfunc=np.sum)
    df_pivot.reset_index(inplace=True)
    df_pivot.rename(columns={'qty_received':'received_sum','purchased_amount':'purchased_amount_sum'},inplace=True)
    #print df_pivot.head()
    #以便后面连接时区分
    df_pr=get_pr_info(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    df_pr=df_pr[['supplier_name','pay_rmb']]
    df_pivot=df_pivot.merge(df_pr,on='supplier_name',how='left')
    df_pivot['freight']=df_pivot['pay_rmb']-df_pivot['purchased_amount_sum']
    #print df_pivot.head()

    df_change=df.merge(df_pivot,on='supplier_name',how='left')
    df_change['purchased_amount_c']=df_change['purchased_amount']+df_change['qty_received']/df_change['received_sum']*df_change['freight']
    df_change['purchased_amount_c']=df_change['purchased_amount_c'].round(2)
    df=df_change

    df=df[['uuid','create_time','supplier_uuid','supplier_name','purchased_amount_c',u'贸易类型','title','qty_received','purchase_price','po_tracking_no','so_tracking_no','delivery_way']]
    df.rename(columns={'uuid':u'订单编号','create_time':u'交易日期','supplier_uuid':u'供应商ID','supplier_name':u'供应商名称','purchased_amount_c':u'交易金额',
                       'title':u'商品名称','qty_received':u'数量','purchase_price':u'单价','po_tracking_no':u'国内物流','so_tracking_no':u'国际物流','delivery_way':u'承运人'},inplace=True)

    #print df.head()
    return  df
def get_pr_info(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
   	SELECT pr.uuid as pr_uuid,pr.supplier_uuid,pr.supplier_name,CONVERT_TZ(pr.create_time,'+00:00','+8:00') as pr_create_time,CONVERT_TZ(pr.pay_time,'+00:00','+8:00') as pay_time ,pr.purchase_rmb,
   	pr.freight_rmb,pr.total_rmb,pr.pay_rmb,bank_name,deposit_bank,bank_account,account_holder,pr.status
	from payment_request as pr 	
	where pr.status='PR_FINANCE_PASS' 
    """

    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    if df.empty:
        exit()
    grouped=df.groupby('supplier_uuid',as_index=False)
    df = grouped.agg({'supplier_name':'first','pay_rmb':'sum','bank_name':'first','deposit_bank':'first','bank_account':'first','account_holder':'first'})
    df=df[['supplier_uuid','supplier_name','pay_rmb','bank_name','deposit_bank','bank_account','account_holder']]

    #print df.head()
    return df

def getPoTrackingNo(uuid):
    sql = """
	select po.uuid,if(ISNULL(st.tracking_no),st2.tracking_no,st.tracking_no) as po_tracking_no
	from purchase_order as po 
    left join purchase_order_shipping_tickets as post on po.id=post.purchaseorder_id
    left join shipping_ticket as st on post.shippingticket_id=st.id
    left join supply_order as so on po.supply_order_id=so.id
    left join shipping_ticket_supply_orders as stso on so.id=stso.supplyorder_id
    left join shipping_ticket as st2 on stso.shippingticket_id=st2.id
	where po.uuid in ({})
    """.format(','.join (uuid))
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)

    return df
def getSoTrackingNo(start,end):
    sql = """
    select distinct sol.order_name,CONVERT_TZ(sol.create_time,'+00:00','+8:00') as create_time,item_no,tracking_no as so_tracking_no,delivery_way
	from sale_order_line as sol 
	join sale_logistics as sl on sol.order_name=sl.order_name
	where  CONVERT_TZ(sol.create_time,'+00:00','+8:00') BETWEEN '{}' and '{}'
    """.format(start,end)
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    return df
if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()

    # start = '2017-11-01'
    # end = '2017-11-30'
    # start = datetime.strptime(start, "%Y-%m-%d").date()
    # end = datetime.strptime(end, "%Y-%m-%d").date()
    today = datetime.now()
    now=datetime(today.year, today.month, today.day, today.hour, today.minute)#为了分辨每次运行生成的excel文件

    df_pr=get_pr_info(idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    df_pr.rename(columns={'supplier_uuid': u'供应商ID', 'supplier_name': u'供应商名称', 'pay_rmb': u'下发金额(RMB)', 'bank_name': u'银行名称',
                 'deposit_bank': u'开户行', 'bank_account': u'银行账号', 'account_holder': u'开户人'}, inplace=True)
    df_prpo=get_prpo_info(idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    path = u'F:\createExcel\对账单审核通过'+str(now)+'.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_pr.to_excel(writer, sheet_name=u'请款', index=False, encoding='utf8')
    df_prpo.to_excel(writer, sheet_name=u'采购', index=False, encoding='utf8')
    writer.save()
