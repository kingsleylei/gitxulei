#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter
import os



# for root,dirs,files in os.walk(path):
#     for name in files:
#         print os.path.join(root,name)

def combine_excel(path):

    for i,file in  enumerate(os.listdir(path)):
        filepath = os.path.join(path, file)
        if i==0:

            df1 =pd.read_excel(filepath,u'质量问题交异常')
            df_sheet1=df1

            df2= pd.read_excel(filepath, u'错款交异常')
            df_sheet2 = df2

            df3 = pd.read_excel(filepath, u'尺寸问题交异常')
            df_sheet3=df3
            #print i

        else:
            df1 = pd.read_excel(filepath, u'质量问题交异常')
            df2 = pd.read_excel(filepath, u'错款交异常')
            df3 = pd.read_excel(filepath, u'尺寸问题交异常')

            df_sheet1 = df_sheet1.append(df1)
            df_sheet2 = df_sheet2.append(df2)
            df_sheet3 = df_sheet3.append(df3)
            #print df_sheet3.head()
    return df_sheet1,df_sheet2,df_sheet3
if __name__ == '__main__':
    path = r'F:\temp data\xsWK46'
    path = unicode(path, 'utf-8')
    df1,df2,df3=combine_excel(path)

    create_path = u'F:\createExcel\combine5.xlsx'
    writer = pd.ExcelWriter(create_path, engine='xlsxwriter', options={'strings_to_urls': False})
    df1.to_excel(writer, u'质量问题交异常', index=False,encoding='utf-8')
    df2.to_excel(writer, u'错款交异常', index=False,encoding='utf-8')
    df3.to_excel(writer,u'尺寸问题交异常', index=False,encoding='utf-8')
    writer.save()

