#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlsxwriter
def get_po_info(start,end,idoo_yh_conn ,idoo_xs_conn,idoo_hn_conn):
    sql="""
    select date_format(CONVERT_TZ(po.create_time,'+00:00','+8:00'),"%Y-%m-%d") as create_date_ch,po.uuid,qty_system_demand,qty_purchase_demand,qty_purchased,qty_received,supply_qyt_sys_demand,supply_qyt_purchased,
    purchase_price,qty_purchased*purchase_price as purchase_amount, CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time_ch,CONVERT_TZ(purchase_time,'+00:00','+8:00') as purchase_time_ch,
    CONVERT_TZ(complete_time,'+00:00','+8:00') as complete_time_ch,CONVERT_TZ(signed_time,'+00:00','+8:00') as signed_time_ch,timestampdiff(SECOND,po.create_time,purchase_time)/3600 as order_interval,
    timestampdiff(SECOND,purchase_time,signed_time)/3600 as logistics_interval,purchase_company_name,is_cooperative_supplier,apply_quantity,return_quantity,gr.status,sku_no,item_no,source_type,po.state,po.prev_state
    from
    (select * from
    (select *
    from purchase_order
    where  CONVERT_TZ(create_time,'+00:00','+8:00') between '{}' and '{}') as b
    left join 
    (select source_purchase_order_id as sp_order_id,sum(qty_system_demand) as supply_qyt_sys_demand,sum(qty_purchased) as supply_qyt_purchased
    from purchase_order group by source_purchase_order_id
    ) as c on b.id=c.sp_order_id) as po 
    left join 	
    goods_return as gr on po.id=gr.purchase_order_id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    """.format(start,end)
    df_yh = pd.read_sql(sql,idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
   # df_all['qty_real_demand']=np.where(df_all['qty_system_demand']!=0,df_all['qty_purchased'],max(df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_all['qty_real_demand'] = np.where(df_all['qty_system_demand']!=0,df_all['qty_purchased'],
                                         np.where(df_all['supply_qyt_sys_demand']>df_all['supply_qyt_purchased'],df_all['supply_qyt_sys_demand'],df_all['supply_qyt_purchased']))
    df_all.loc[df_all['status']==3,'apply_quantity']=0
    #print df_all.head(10)
   # df_all.to_excel('F:\po_KPI_rawdata3.xlsx',index=False)
    grouped=df_all.groupby('item_no',as_index=False)
    #df=grouped['qty_system_demand','qty_purchase_demand','qty_purchased','qty_received','apply_quantity','order_interval','logistics_interval'].sum()
    df=grouped.agg({'qty_real_demand':'sum','qty_system_demand':'sum','qty_purchased':'sum','qty_received':'sum','apply_quantity':'sum','order_interval':'sum','logistics_interval':'sum','uuid':'count'})
   # df['shortage_num']=df['qty_real_demand']-df['qty_received']
   # df['shortage_rate']=(df['qty_system_demand']-df['qty_received'])/df['qty_system_demand']
    df['shortage_rate']=(df['qty_purchased']-df['qty_received'])/df['qty_purchased']
    df['return_rate']=df['apply_quantity']/df['qty_purchased']
    #df['return_rate'] = df['apply_quantity'] / df['qty_system_demand']
    df['avg_order_interval']=df['order_interval']/df['uuid']
    df['avg_logistics_interval']=df['logistics_interval']/df['uuid']
    df=df.fillna(0)
    df=df.sort_values(by='shortage_rate',ascending=False)
    df=df[['qty_purchased','qty_received','item_no','shortage_rate','return_rate','avg_order_interval','avg_logistics_interval']]
    return df


def company_sku(idoo_yh_conn ,idoo_xs_conn,idoo_hn_conn,supplier_conn):

    #supply info
    sql="""
    select sku_id,sku_no,item_no
    from stock_keep_unit as sku join product as pro on sku.product_id=pro.id
    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all=df_all.drop_duplicates()

    supply_sql="""
    select company_name,stock_keep_unit_id as sku_id
    from link join sku_link_rel as sll on link.id=sll.link_id

    """
    df_sup=pd.read_sql(supply_sql,supplier_conn)
    df=df_sup.merge(df_all,on='sku_id',how='inner')
    df['company_name']= df['company_name'].str.strip()
    grouped=df.groupby('company_name',as_index=False)
    df=grouped['sku_no','item_no'].agg(lambda arr:arr.drop_duplicates().size)
    df.to_excel('F:\createExcel\company_sku2.xlsx')
    print df.head(10)
    # df=df.merge(df_sup,left_on='purchase_company_name',right_on='company_name',how='left')
    # df['item_out_rate']=df['sku_id']/df['sku_num']

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()
    start='2017-09-01'
    end='2017-11-01'
    start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
    end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
    df_all=get_po_info(start,end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    #company_sku(idoo_yh_conn,idoo_xs_conn,idoo_hn_conn,conn)

    path = r'F:\createExcel\shortageByItem5'+str(start)+'-'+str(end)+'.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_all.to_excel(writer, index=False, encoding='utf8')
    writer.save()