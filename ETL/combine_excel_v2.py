#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter
import xlrd
import os




# for root,dirs,files in os.walk(path):
#     for name in files:
#         print os.path.join(root,name)
# header1=[u'日期','批次','异常处理原因',u'处理结果',u'处理备注']
# header2=[u'日期','批次','异常处理原因',u'处理结果',u'处理备注']
# header3=[u'日期']
def combine_excel(path,create_path):
    workbook = xlsxwriter.Workbook(create_path)
    cb_sheet1 = workbook.add_worksheet(u'质量问题交异常')
    cb_sheet2 = workbook.add_worksheet(u'错款交异常')
    cb_sheet3 = workbook.add_worksheet(u'尺寸问题交异常')
    m1=[]
    m2=[]
    m3=[]
    m4=[]
    for file in  os.listdir(path):
        filepath=os.path.join(path,file)
        print filepath
        wb=xlrd.open_workbook(filepath)

        sheet1=wb.sheet_by_name(u'质量问题交异常')
        sheet2 = wb.sheet_by_name(u'错款交异常')
        sheet3 = wb.sheet_by_name(u'尺寸问题交异常')
         # 列表存储数据
        for i in range(1, sheet1.nrows):
            m1 = []
            for j in range(sheet1.ncols):
                m1.append(sheet1.cell(i, j).value)

                # cb_sheet1.write(i,j,sheet1.cell(i,j).value)
            m2.append(m1)
            # print m2
        for i in range(1, sheet2.nrows):
            m1 = []
            for j in range(sheet2.ncols):
                m1.append(sheet2.cell(i, j).value)

                # cb_sheet1.write(i,j,sheet1.cell(i,j).value)
            m3.append(m1)
            # print m3
        for i in range(1, sheet3.nrows):
            m1 = []
            for j in range(sheet3.ncols):
                m1.append(sheet3.cell(i, j).value)

                # cb_sheet1.write(i,j,sheet1.cell(i,j).value)
            m4.append(m1)
        # 写入标题行

        for j in range(sheet1.ncols):
            cb_sheet1.write(0, j, sheet1.cell(0, j).value)
        for j in range(sheet2.ncols):
            cb_sheet2.write(0, j, sheet2.cell(0, j).value)
        for j in range(sheet3.ncols):
            cb_sheet3.write(0, j, sheet3.cell(0, j).value)

        # 写入内容

    # print m4
    for i in range(len(m2)):
        for j in range(len(m2[i])):
            cb_sheet1.write(i + 1, j, m2[i][j])
    for i in range(len(m3)):
        for j in range(len(m3[i])):
            cb_sheet2.write(i + 1, j, m3[i][j])
    for i in range(len(m4)):
        for j in range(len(m4[i])):
            cb_sheet3.write(i + 1, j, m4[i][j])

def rd_wt(path):
    for file in  os.listdir(path):
        filepath=os.path.join(path,file)
        wb=xlrd.open_workbook(filepath)
        sheet1=wb.sheet_by_name(u'质量问题交异常')
        sheet2 = wb.sheet_by_name(u'错款交异常')
        sheet3 = wb.sheet_by_name(u'尺寸问题交异常')
         # 列表存储数据
        for i in range(1, sht.nrows):
            m1 = []
            for j in range(sht.ncols):
                m1.append(sht.cell(i, j).value)

                # cb_sheet1.write(i,j,sheet1.cell(i,j).value)
            m2.append(m1)
            # print m2
            # 写入标题行
    for j in range(sht.ncols):
        cb_sheet.write(0, j, sht.cell(0, j).value)
        # 写入内容
    for i in range(len(m2)):
        for j in range(len(m2[i])):
            cb_sheet.write(i + 1, j, m2[i][j])


if __name__ == '__main__':
    path = r'F:\temp data\xyWK47'
    path = unicode(path, 'utf-8')

    create_path=r'F:\temp data\combine\xyWK47.xlsx'
    combine_excel(path,create_path)

    # create_path = u'F:\createExcel\combine5.xlsx'
    # writer = pd.ExcelWriter(create_path, engine='xlsxwriter', options={'strings_to_urls': False})
    # df1.to_excel(writer, u'质量问题交异常', index=False,encoding='utf-8')
    # df2.to_excel(writer, u'错款交异常', index=False,encoding='utf-8')
    # df3.to_excel(writer,u'尺寸问题交异常', index=False,encoding='utf-8')
    # writer.save()

