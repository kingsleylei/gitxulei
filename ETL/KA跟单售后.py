#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter


def get_purchased(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select distinct CONVERT_TZ(po.create_time,'+00:00','+8:00') as create_time,date(CONVERT_TZ(po.create_time,'+00:00','+8:00')) as create_date,po.uuid,sku.sku_no,pro.item_no,pro.title,po.state,qty_system_demand,qty_purchased,qty_received,
    if(length(external_order_id)=17,right(external_order_id,4),'') as 1688_account,purchase_company_name,is_cooperative_supplier,po.logistics_status
    from purchase_order as po
    left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    left join supply_order as so on po.supply_order_id=so.id
    where state in (1,2,3)  and is_cooperative_supplier=1 
    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all.drop_duplicates('uuid',inplace=True)

    pur_t4=df_all.loc[end-df_all['create_date']>=timedelta(days=4),'uuid'].count()
    pur_t7= df_all.loc[end - df_all['create_date'] >= timedelta(days=7), 'uuid'].count()
    return df_all,pur_t4,pur_t7

def get_returned(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select CONVERT_TZ(gr.create_time,'+00:00','+8:00') as create_time,date(CONVERT_TZ(po.create_time,'+00:00','+8:00')) as create_date,application_id,
    apply_quantity,status,po.uuid,sku_no,item_no,purchase_company_name,GROUP_CONCAT(reason_detail SEPARATOR ' ') as reason_detail,qty_system_demand,qty_purchased,qty_received
    from goods_return as gr
    join return_reason as rr on gr.id=rr.return_application_id
    join purchase_order as po on gr.purchase_order_id=po.id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    WHERE CONVERT_TZ(gr.create_time,'+00:00','+8:00')<'{}' and is_cooperative_supplier=1
    group by application_id

    """.format(end)

    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)

    unPro_t3=df_all.loc[(df_all['create_date']+timedelta(days=3)<=end)&(df_all['status']==0),'uuid'].count()

    df=df_all
    fillAddr_t1=df.loc[(end-df['create_date']>=timedelta(days=1))&(df['status']==10),'uuid'].count()
    return df_all,unPro_t3,fillAddr_t1

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()


    end = datetime.now().date()

    df_pur, pur_t4, pur_t7= get_purchased(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)
    df_re, unPro_t3, fillAddr_t1=get_returned(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn)

    df_stat = pd.DataFrame({u'<=T-7已采购':pur_t7,u'<=T-4已采购':pur_t4,u'<=T-3未处理':unPro_t3,u'填地址':fillAddr_t1},index=[datetime.now().date()],
                           columns=[u'<=T-7已采购',u'<=T-4已采购',u'<=T-3未处理',u'填地址'])
    # #print df_all.head()
    path = u'F:\createExcel\KA跟单售后'+str(datetime.now().date())+'.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_pur.to_excel(writer,sheet_name=u'已采购',index=False,encoding= 'utf8')
    df_re.to_excel(writer, sheet_name=u'退货', index=False, encoding='utf8')
    df_stat.to_excel(writer, sheet_name=u'统计', index=False, encoding='utf8')
    writer.save()