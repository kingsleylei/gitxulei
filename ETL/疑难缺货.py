#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter

def get_diff_order():

    sql = """
    select date(CONVERT_TZ(po.create_time,'+00:00','+8:00')) as create_date,po.uuid,sku_no,item_no,title,qty_system_demand,
    qty_purchased,purchase_company_name,sku_difficult_note,purchase_note,state,source_type,difficult_order_follower_name,category_name,purchase_link
    from purchase_order as po 
    left join purchase_order_notes as pon on po.id=pon.purchase_order_id
    left join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    where difficult_order=1 and state=0 and purchase_note not like '%有货%'
    """


    warehouse = [u'余杭', u'萧山', u'海宁']
    df = pd.DataFrame()
    for con, wh in zip(conn, warehouse):
        df_temp = pd.read_sql(sql, con)
        df_temp['warehouse'] = wh
        df = df.append(df_temp, ignore_index=True)
    df['purchase_note']=df['purchase_note'].str.replace(r'[=+-@]','')
    df['sku_difficult_note'] = df['sku_difficult_note'].str.replace(r'[=+-@]', '')
    df_g=df.groupby('item_no',as_index=False).agg({'sku_no':'first','category_name':'first','qty_system_demand':'sum','purchase_company_name':'first','purchase_link':'first'})
    df_g.rename(columns={'qty_system_demand':'total_demand'},inplace=True)
    df=df.merge(df_g,on='item_no',how='left')

    return df,df_g
if __name__ == '__main__':

    conn=db_help.get_idoo_conn()
    df, df_g=get_diff_order()
    path = u'F:\createExcel\疑难缺货' + str(datetime.now().date()) + '.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df.to_excel(writer, sheet_name=u'疑难缺货raw', index=False, encoding='utf8')
    df_g.to_excel(writer, sheet_name=u'疑难缺货统计', index=False, encoding='utf8')
    writer.save()