#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
from datetime import datetime,timedelta,date
import xlwt
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from cStringIO import StringIO

base_path = os.path.dirname(os.path.realpath(__file__))


def send_mail(excel_files, recipients):
    username = 'leixu@epiclouds.net'
    password = 'Uei1506892'
    sender = username
    receivers = ", ".join(recipients)

    # 如名字所示： Multipart就是多个部分
    msg = MIMEMultipart()
    msg['Subject'] = 'Daily Data Report'
    msg['From'] = sender
    msg['To'] = receivers

    # 正文
    puretext = MIMEText(u'Attached please find the latest data report.', _charset='UTF-8')
    msg.attach(puretext)

    # xlsx类型的附件
    for excel_file in excel_files:

        # try:
        xlsxpart = MIMEApplication(open(excel_file, 'rb').read())
        xlsxpart.add_header('Content-Disposition', 'attachment',
                            filename=excel_file.split('/')[-1].encode('gbk'))
        msg.attach(xlsxpart)
        # except IOError:
        #     print '要添加的附件本身不存在'

    try:
        host = 'smtp.mxhichina.com'
        port = 465
        client = smtplib.SMTP_SSL(host, port)
        client.login(username, password)
        client.sendmail(sender, receivers, msg.as_string())
        client.quit()
        print 'yeah'
    except smtplib.SMTPRecipientsRefused:
        print 'Recipient refused'
    except smtplib.SMTPAuthenticationError:
        print 'Auth error'
    except smtplib.SMTPSenderRefused:
        print 'Sender refused'
    except smtplib.SMTPException, e:
        print e.message
    except Exception, e:
        print e.message

if __name__ == '__main__':
    today = str(date.today())
    yesterday = str(date.today() - timedelta(1))
    path=u'F:/createExcel'
   #execfile("purchase_KPI.py")
    # supply_files = []
    # supply_files.append(path + u"/采购发货率{}.xlsx".format(today))
    # send_mail(supply_files, ['gongyingshang@epiclouds.net'])

    yangzhenxi_files=[]
    yangzhenxi_files.append(path +u"/采购指标{}.xlsx".format(today))
    send_mail(yangzhenxi_files, ['zhenxiyang@epiclouds.net'])
    #
    # yao_files = []
    # yao_files.append(u"E:/{}".format(yesterday)+u"类目交易数据.xlsx")
    # #print u"E:\\{}类目交易数据".format(yesterday)
    # send_mail(yao_files, ['shangpinyunying@epiclouds.net'])
