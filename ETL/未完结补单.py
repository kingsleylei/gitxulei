#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter


def get_po_rawdata(idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select repo.uuid,repo.qty_system_demand,repo.qty_purchased,repo.qty_received,CONVERT_TZ(repo.create_time,'+00:00','+8:00') as create_time,CONVERT_TZ(repo.purchase_time,'+00:00','+8:00') as purchase_time,
    CONVERT_TZ(repo.complete_time,'+00:00','+8:00') as complete_time,CONVERT_TZ(repo.signed_time,'+00:00','+8:00') as signed_time,repo.state,repo.prev_state,repo.source_type,repo.purchase_company_name,po.uuid as og_uuid,CONVERT_TZ(po.create_time,'+00:00','+8:00') as og_create_time,
    po.qty_system_demand as og_system_demand,po.qty_purchased as og_purchased,po.is_cooperative_supplier as og_cooperative_supplier,category_name,if(length(external_order_id)=17,right(external_order_id,4),'') as 1688_account,
    if(po.qty_purchased !=0,po.qty_purchased,GREATEST(repo.qty_system_demand,repo.qty_purchased)) as real_demand,po.purchase_company_name as og_company,item_no
    from
    (select *
    from purchase_order
    where is_cooperative_supplier=0 and state in (0,1,2,3)  and source_type in (0,2))
    as repo 
    left join purchase_order as po on repo.top_source_purchase_order_id=po.id
    left join stock_keep_unit as sku on repo.stock_keep_unit_id=sku.id
    left join product as pro on sku.product_id=pro.id
    left join supply_order as supo on repo.supply_order_id = supo.id

    """
    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'

    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'

    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all['create_time']=[t.date()for t in df_all['create_time']]
    df_all['og_create_time'] = [t.date() for t in df_all['og_create_time']]
    return df_all
if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()

    df=get_po_rawdata(idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)
    #print df_all.head()
    path = u'F:\createExcel\未完结补单2.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df.to_excel(writer,sheet_name=u'未完结补单',index=False,encoding= 'utf8')
    writer.save()