#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import db_help
import xlsxwriter


def get_returned(end,idoo_yh_conn, idoo_xs_conn, idoo_hn_conn):
    sql = """
    select CONVERT_TZ(gr.create_time,'+00:00','+8:00') as create_time,application_id,apply_quantity,status,po.uuid,sku_no,item_no,purchase_company_name,GROUP_CONCAT(reason_detail SEPARATOR ' ') as reason_detail,qty_system_demand,qty_purchased,qty_received
    from goods_return as gr
    join return_reason as rr on gr.id=rr.return_application_id
    join purchase_order as po on gr.purchase_order_id=po.id
    join stock_keep_unit as sku on po.stock_keep_unit_id=sku.id
    join product as pro on sku.product_id=pro.id
    WHERE CONVERT_TZ(gr.create_time,'+00:00','+8:00')<'{}' and is_cooperative_supplier=0
    group by application_id

    """.format(end)

    df_yh = pd.read_sql(sql, idoo_yh_conn)
    df_yh['warehouse'] = u'余杭'
    df_xs = pd.read_sql(sql, idoo_xs_conn)
    df_xs['warehouse'] = u'萧山'
    df_hn = pd.read_sql(sql, idoo_hn_conn)
    df_hn['warehouse'] = u'海宁'
    df_all = pd.concat([df_yh, df_xs, df_hn], ignore_index=True)
    df_all['create_time']=pd.to_datetime(df_all['create_time'])

    m=df_all.loc[(df_all['status']!=0)&(end-df_all['create_time']<=timedelta(days=1)),'uuid'].count()
    n=df_all.loc[end-df_all['create_time']<=timedelta(days=1),'uuid'].count()  #昨天生成的退款单
    handleRate=m/n
    #T-2未处理+T-1生成-T-1未处理=T-1一整天处理了多少条
    p=n+df_all.loc[(df_all['create_time']+timedelta(days=1)<end)&(df_all['status']==0),'uuid'].count()-df_all.loc[(df_all['create_time']<end)&(df_all['status']==0),'uuid'].count()
    q=n+df_all.loc[(df_all['create_time']+timedelta(days=1)<end)&(df_all['status']==0),'uuid'].count()#t-2以前未处理+t-1当天生成
    handleRateAll=p/q
    #print m,n,p,q, handleRate,handleRateAll


    #计算t-5以前的退款单填地址的状态在t-1的的完成率，现在只能分两天跑程序计算
    df=df_all
    t4=df.loc[(end-df['create_time']>timedelta(days=3))&(df['status']==10),'uuid'].count()
    t5=df.loc[(end-df['create_time']>timedelta(days=4)) & (df['status'] == 10), 'uuid'].count()#包括t-5当天
    #回填物流所有
    z=df.loc[(end-df['create_time']>timedelta(days=0)) & (df['status'] == 22), 'uuid'].count()
    #df_all['create_time']=[d.date() for d in df_all['create_time']]
    #print m, n, p, q, handleRate, handleRateAll
    df_stat = pd.DataFrame({u'T-1生成完成':m,u'T-1生成':n,u'T-1生成完成率':handleRate,u'T-1当日总完成':p,u'T-1总未处理':q,
                            u'T-1总计完成率':handleRateAll,u'T-4填地址': t4,u'T-5填地址': t5,u'回填物流': z}, index=[end],
                           columns=[u'T-1生成完成',u'T-1生成',u'T-1生成完成率',u'T-1当日总完成',u'T-1总未处理', u'T-1总计完成率',u'T-4填地址',u'T-5填地址',u'回填物流'])
    return df_all,df_stat

if __name__ == '__main__':
    idoo_yh_conn = db_help.get_new_idoo_yh_conn()
    idoo_xs_conn = db_help.get_new_idoo_xs_conn()
    idoo_hn_conn  =  db_help.get_new_idoo_hn_conn()
    conn=db_help.get_idoo_supplier()

    end=datetime.now().date()

    df_all, df_stat=get_returned(end,idoo_yh_conn,idoo_xs_conn,idoo_hn_conn)


    path = u'F:\createExcel\退货处理5'+str(end)+'.xlsx'
    writer = pd.ExcelWriter(path, engine='xlsxwriter', options={'strings_to_urls': False})
    df_all.to_excel(writer,sheet_name=u'rawdata',index=False,encoding= 'utf8')
    df_stat.to_excel(writer, sheet_name=u'统计', index=True, encoding='utf8')
    writer.save()