# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 17:16:56 2017

@author: lenovo
"""

##odoo_own
import pandas as pd
import psycopg2

def get_odoo_conn():
    conn1 = psycopg2.connect(
        user='root',
        password='123Yuanshuju456',
        host='120.27.152.95',
        port=5432,
        database='odoo_own')
    return conn1
################id与goods与cate_name的对应关系##########################
def get_id_goods(odoo_conn,ppc_id_list,cate_id,p_id):
    sql_product ='''
    select ppcptr.product_public_category_id as {}, pt.product_no as item_no,ppc.parent_id as {} ,ppc.name
    from product_template as pt 
    join product_public_category_product_template_rel as ppcptr on pt.id=ppcptr.product_template_id 
    join product_public_category as ppc on ppc.id = ppcptr.product_public_category_id
    where ppcptr.product_public_category_id in ({}) 
    and pt.product_no <>'' 
    '''.format(cate_id,p_id,','.join("'"+ str(pp_id) +"'" for pp_id in ppc_id_list )) ###and pt.sale_ok = 't'
    id_goods= pd.read_sql(sql_product,con=odoo_conn)
    return id_goods
################id与类目的对应关系##########################
def get_id_cate(odoo_conn):
    #1级类目与id对应关系
    sql_name ='''
    select id as id_one, name as category_one from product_public_category where  parent_id is  null
    '''
    cate_one= pd.read_sql(sql_name,con=odoo_conn)  
    #2级类目与id对应关系
    parent_id = cate_one['id_one']
    sql ='''
     SELECT id as id_two,name as category_two,parent_id as id_one from product_public_category as ppc WHERE 
     parent_id in ({}) and parent_id is not null
     '''.format(','.join("'"+str(b)+"'" for b in parent_id))
    cate_two = pd.read_sql(sql,con=odoo_conn)
    #3级类目与id对应关系
    parent_id_second = cate_two['id_two']
    sql_three ='''
     SELECT id as id_three,name as category_three,parent_id as id_two from product_public_category as ppc 
     WHERE parent_id in ({}) and parent_id is not null
     '''.format(','.join("'"+str(b)+"'" for b in parent_id_second))
    cate_three = pd.read_sql(sql_three,con=odoo_conn)
    
    return cate_one,cate_two,cate_three

def goods_category():
    odoo_conn = get_odoo_conn()
    #####cate与id对应
    cate_one,cate_two,cate_three = get_id_cate(odoo_conn)
    #####cate与一级id对应与item对应
    id_one = cate_one['id_one'] 
    cate_id1 = "id_one"
    p_id1 = 'id_no'
    id_cateone_pno = get_id_goods(odoo_conn,id_one,cate_id1,p_id1)
    del id_cateone_pno['id_no']
    print id_cateone_pno.shape
    #####cate与二级id对应与item对应
    id_two = cate_two['id_two'] 
    cate_id2 = "id_two"
    p_id2 = 'id_one'
    id_catetwo_pno = get_id_goods(odoo_conn,id_two,cate_id2,p_id2)
    print id_catetwo_pno.shape
    #####cate与三级id对应与item对应
    id_three = cate_three['id_three']
    cate_id3 = "id_three"
    p_id3 = 'id_two'
    id_catethree_pno = get_id_goods(odoo_conn,id_three,cate_id3,p_id3)
    print id_catethree_pno.shape
    
    #####创建最终所取表
    cate = pd.DataFrame(columns = ['item_no','category_one','category_two','category_three'])
    #####三级类目商品
    item3 = pd.merge(id_catethree_pno,cate_two,on = 'id_two',how = 'inner')
    item3 = pd.merge(item3,cate_one,on = 'id_one',how = 'inner')
    del item3['id_three']
    del item3['id_two']
    del item3['id_one']
    item3 = item3.rename(columns={'name':'category_three'})
    item3 = item3[['item_no','category_one','category_two','category_three']]
    cate = cate.append(item3)
    print cate.shape
    
    #####二级类目商品
    three_item = item3['item_no']
    item2 = id_catetwo_pno[-id_catetwo_pno['item_no'].isin(three_item)]
    item2 = pd.merge(item2,cate_one,on = 'id_one',how = 'inner')
    del item2['id_two']
    del item2['id_one']
    item2 = item2.rename(columns={'name':'category_two'})
    item2['category_three'] = ''
    item2 = item2[['item_no','category_one','category_two','category_three']]
    cate = cate.append(item2)
    print cate.shape
    #####一级商品类目
    two_item = item2['item_no']
    item1 = id_cateone_pno[-id_cateone_pno['item_no'].isin(two_item)]
    item1 = item1[-item1['item_no'].isin(three_item)]
    del item1['id_one']
    item1 = item1.rename(columns={'name':'category_one'})
    item1['category_two'] = ''
    item1['category_three'] = ''
    item1 = item1[['item_no','category_one','category_two','category_three']]
    cate = cate.append(item1)
    cate = cate.drop_duplicates()
    print cate.shape
    return cate

if __name__ == '__main__':
    cate = goods_category()
    cate.to_excel('F:\catenew.xlsx')
