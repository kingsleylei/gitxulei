#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import db_help
import xlrd
import xlwt

#获得货号，品类id的对应表
def get_product_no(odoo_conn):
    sql="""
        select pt.product_no,ppcptr.product_public_category_id as ppc_id
        from product_template as pt left join product_public_category_product_template_rel as ppcptr 
        on pt.id=ppcptr.product_template_id
        where length(trim(product_no))>1 
    """
    df_proNO=pd.read_sql(sql,odoo_conn)
    return df_proNO

#获得货号与各级品类名对应表
def get_category(odoo_conn):
    sql_category_one = '''
    SELECT 
            distinct id as id_one, name as Category_one 
    from 
            product_public_category as ppc 
    WHERE 
            parent_id is  null
   '''
    #获取一级品类表
    cate_one = pd.read_sql(sql_category_one, con=odoo_conn)

    parent_id = cate_one['id_one']
    sql = '''
    SELECT 
            distinct id as id_two,name as Category_two ,parent_id as id_one 
    from 
            product_public_category as ppc 
    WHERE 
            parent_id in ({})
    '''.format(','.join("'" + str(b) + "'" for b in parent_id))
    #获得二级品类表
    cate_two = pd.read_sql(sql, con=odoo_conn)

    parent_id_second = cate_two['id_two']
    sql_three = '''
    SELECT 
            distinct id as id_three,name as Category_three ,parent_id as id_two 
    from 
            product_public_category as ppc 
    WHERE 
            parent_id in ({})
     '''.format(','.join("'" + str(b) + "'" for b in parent_id_second))
    #获得三级品类表
    cate_three = pd.read_sql(sql_three, con=odoo_conn)


    #定义最终要生成的货号品类对应表
    df_all=pd.DataFrame(columns=['product_no','category_one','category_two','category_three'])
    #导入货号品类id对应表
    df_proNo=get_product_no(odoo_conn)
    print df_proNo.shape

    #先找出有三级类目的货号
    df3 = df_proNo.merge(cate_three, left_on='ppc_id', right_on='id_three', how='inner')
    df3=df3.merge(cate_two,on='id_two',how='inner')
    df3=df3.merge(cate_one,on='id_one',how='inner')
    print df3.shape
    df_all=df_all.append(df3[['product_no','category_one','category_two','category_three']])

    #找出有二级类目的货号
    df2=df_proNo.merge(cate_two, left_on='ppc_id', right_on='id_two', how='inner')
    df2 = df2.merge(cate_one, on='id_one', how='inner')
    df2['category_three']=''
    print df2.shape
    df_all=df_all.append(df2[['product_no','category_one','category_two','category_three']])

    #找出一级类目货号
    df1=df_proNo.merge(cate_one, left_on='ppc_id', right_on='id_one', how='inner')
    df1['category_two']=''
    df1['category_three'] = ''
    print df1.shape
    df_all=df_all.append(df1[['product_no','category_one','category_two','category_three']])

    #去重，保留首次出现的货号，这一步非常重要
    df_all=df_all.drop_duplicates('product_no','first')

    return df_all

if __name__=='__main__':
   odoo_conn=db_help.get_odoo_conn()
   df_all=get_category(odoo_conn)
   #df_all.to_excel('F:\createExcel\get_cate3.xlsx')
#print df
   path = 'F:/createExcel/get_pro_cate3.xlsx'
   writer = pd.ExcelWriter(path)
   df_all.to_excel(writer,index=False, encoding='utf8')
   writer.save()